======
WXWork
======

WXWork is a Python library for `WeChat Work`_ administrators.
It offers simple, flexible access to many `WeChat Work APIs`_.


.. _`WeChat Work`: https://work.weixin.qq.com
.. _`WeChat Work APIs`: https://work.weixin.qq.com/api/doc