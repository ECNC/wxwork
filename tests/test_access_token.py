from unittest import TestCase
from unittest.mock import patch

from wxwork.api import AccessTokenManager


class TestAccessToken(TestCase):
    TOKEN_RESP = {
        "errcode": 0, "errmsg": "ok",
        "access_token": "pB8wS1NJiOhOeiRhp9puQkc8pJdfAb8Qg1aIXkd-GSor0nvzEzxApkHr_fz8uv",
        "expires_in": 7200
    }
    ANOTHER_RESP = {
        "errcode": 0, "errmsg": "ok",
        "access_token": "------NJiOhOeiRhp9puQkc8pJdfAb8Qg1aIXkd-GSor0nvzEzxApkHr_fz8",
        "expires_in": 7200
    }

    def setUp(self):
        self.access_token_manager = AccessTokenManager("corp_id", "secret")

    @patch('time.time', return_value=10000.123)
    @patch('wxwork.http.request', return_value=TOKEN_RESP)
    def test_get_access_token(self, mock_request, mock_time):
        access_token = self.access_token_manager.get_access_token()
        self.assertEqual(self.TOKEN_RESP["access_token"], access_token)
        mock_request.assert_called_once_with(
            "gettoken", {"corpid": "corp_id", "corpsecret": "secret"}, None, None
        )
        self.assertEqual(int(mock_time.return_value) + self.TOKEN_RESP["expires_in"],
                         self.access_token_manager.expires_at)

    @patch('time.time', return_value=1000.123)
    @patch('wxwork.http.request', return_value=TOKEN_RESP)
    def test_access_token_cache(self, mock_request, mock_time):
        access_token = self.access_token_manager.access_token
        self.assertEqual(self.TOKEN_RESP["access_token"], access_token)
        self.assertEqual(1, mock_request.call_count)

        mock_request.reset_mock()
        expires_at = self.access_token_manager.expires_at
        mock_time.return_value = expires_at - 1
        cached_access_token = self.access_token_manager.get_access_token()
        self.assertEqual(cached_access_token, access_token)
        self.assertFalse(mock_request.called)
        self.assertEqual(expires_at, self.access_token_manager.expires_at)

        mock_request.reset_mock()

        mock_request.return_value = self.ANOTHER_RESP
        mock_time.return_value = expires_at
        new_access_token = self.access_token_manager.access_token
        self.assertEqual(1, mock_request.call_count)
        self.assertEqual(self.ANOTHER_RESP["access_token"], new_access_token)
        self.assertEqual(expires_at + self.ANOTHER_RESP["expires_in"],
                         self.access_token_manager.expires_at)

    @patch('wxwork.http.request', return_value=TOKEN_RESP)
    def test_get_force_refresh(self, mock_request):
        invalid_token = self.access_token_manager.get_access_token()

        mock_request.return_value = self.ANOTHER_RESP
        access_token = self.access_token_manager.get_access_token(invalid_token)
        self.assertEqual(self.ANOTHER_RESP["access_token"], access_token)

        mock_request.reset_mock()
        cached_access_token = self.access_token_manager.get_access_token()
        self.assertFalse(mock_request.called)
        self.assertEqual(cached_access_token, access_token)
