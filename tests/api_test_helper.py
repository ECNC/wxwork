from collections import namedtuple
from unittest.mock import MagicMock


class DotDict(dict):
    def __getattr__(self, item):
        if item in self:
            return self[item]
        raise AttributeError


File = namedtuple('UploadFile', ['filename', 'data', 'mime'])


class InvokeMock(MagicMock):
    def _mock_check_sig(_mock_self, api_path, json=None, file=None, **query):
        _mock_self.api_path = api_path
        _mock_self.json = DotDict(json) if json is not None else None
        _mock_self.file = File._make(file) if file is not None else None
        _mock_self.query = DotDict(query)
