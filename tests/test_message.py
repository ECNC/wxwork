from unittest import TestCase
from xml.etree.ElementTree import XML

from wxwork import message


class TestMessage(TestCase):
    def test_parse_text(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512923919</CreateTime>
        <MsgType>text</MsgType>
        <MsgId>2135405819</MsgId>
        <Content><![CDATA[<& Test Content &>]]></Content>
        <AgentID>9</AgentID>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Text)
        self.assertEqual(msg.corp_id, 'ww100000a5f2191')
        self.assertEqual(msg.user, 'wangn48')
        self.assertEqual(msg.create_time, 1512923919)
        self.assertEqual(msg.type, 'text')
        self.assertEqual(msg.msg_id, 2135405819)
        self.assertEqual(msg.agent_id, 9)
        self.assertEqual(msg.content, "<& Test Content &>")

    def test_parse_image(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512924401</CreateTime>
        <MsgType>image</MsgType>
        <PicUrl>http://p.qpic.cn/pic_wework/2134032977/acaa84a/</PicUrl>
        <MsgId>2135405819</MsgId>
        <MediaId>1ZdwIm9WlZsEGm709rX3EH07h6dsRMg2owNKF3BQV_QGjcNxzrZ-pwKgg2ZDtbD65</MediaId>
        <AgentID>9</AgentID>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Image)
        self.assertEqual(msg.corp_id, 'ww100000a5f2191')
        self.assertEqual(msg.user, 'wangn48')
        self.assertEqual(msg.create_time, 1512924401)
        self.assertEqual(msg.type, 'image')
        self.assertEqual(msg.msg_id, 2135405819)
        self.assertEqual(msg.media_id, '1ZdwIm9WlZsEGm709rX3EH07h6dsRMg2owNKF3BQV_QGjcNxzrZ-pwKgg2ZDtbD65')
        self.assertEqual(msg.image_url, 'http://p.qpic.cn/pic_wework/2134032977/acaa84a/')
        self.assertEqual(msg.agent_id, 9)

    def test_parse_voice(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512926132</CreateTime>
        <MsgType>voice</MsgType>
        <MediaId>1ksirRVCiigthIP7kYfAzbLWo4HHT21-YdBIPI194Fp4</MediaId>
        <Format>amr</Format>
        <MsgId>551590482</MsgId>
        <AgentID>9</AgentID>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Voice)
        self.assertEqual(msg.type, 'voice')

    def test_parse_video(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512926826</CreateTime>
        <MsgType>video</MsgType>
        <MediaId>1JepAwI_1qNMNFnbs-M6vPCr6-3iuolohrC_rUuettG7UwBDDLlGzYvHickoDodyo</MediaId>
        <ThumbMediaId>1HmqUWQ3giuiaE95L-siaFY541--l6GCWM9N8YarBhTs</ThumbMediaId>
        <MsgId>1131524949</MsgId>
        <AgentID>9</AgentID>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Video)
        self.assertEqual(msg.type, 'video')

    def test_parse_locatin(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512925295</CreateTime>
        <MsgType>location</MsgType>
        <Location_X>23.0666</Location_X>
        <Location_Y>113.377</Location_Y>
        <Scale>16</Scale>
        <Label>Luo Xuan Da Dao, Panyu Qu, Guangzhou Shi, Guangdong Sheng, China</Label>
        <MsgId>485424888</MsgId>
        <AgentID>9</AgentID>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Location)
        self.assertEqual(msg.type, 'location')

    def test_parse_link(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512926689</CreateTime>
        <MsgType>link</MsgType>
        <Title>随便什么TITLe</Title>
        <Description>随便什么DESCRIPTION</Description>
        <Url>http://mp.weixin.qq.com/</Url>
        <PicUrl>http://mmbiz.qpic.cn/mmbiz_jpg/ho1qlz1A8NWWWJt7XicIVt1GY6Tph6</PicUrl>
        <MsgId>1181892160</MsgId>
        <AgentID>9</AgentID>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Link)
        self.assertEqual(msg.type, 'link')

    def test_parse_subscribe(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512927184</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>subscribe</Event>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Subscribe)
        self.assertEqual(msg.type, 'event')
        self.assertEqual(msg.event, 'subscribe')

    def test_parse_unsubscribe(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512927079</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>unsubscribe</Event>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Unsubscribe)
        self.assertEqual(msg.event, 'unsubscribe')

    def test_parse_enter_agent(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512919143</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>enter_agent</Event>
        <EventKey />
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.EnterAgent)
        self.assertEqual(msg.event, 'enter_agent')

    def test_parse_location_repor(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512919369</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>LOCATION</Event>
        <Latitude>22.0628</Latitude>
        <Longitude>110.387</Longitude>
        <Precision>0</Precision>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.LocationReport)
        self.assertEqual(msg.event, 'location')
        self.assertEqual(msg.latitude, 22.0628)
        self.assertEqual(msg.longitude, 110.387)

    def test_parse_click(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512922693</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>click</Event>
        <EventKey>MenuID1</EventKey>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Click)
        self.assertEqual(msg.event, 'click')
        self.assertEqual(msg.event_key, 'MenuID1')

    def test_parse_view(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512922170</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>view</Event>
        <EventKey>http://example.com</EventKey>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.View)
        self.assertEqual(msg.event, 'view')
        self.assertEqual(msg.event_key, 'http://example.com')

    def test_parse_scancode_push(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512922950</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>scancode_push</Event>
        <EventKey>MenuID2</EventKey>
        <ScanCodeInfo>
            <ScanType>barcode</ScanType>
            <ScanResult>EAN_13,6911988011633</ScanResult>
        </ScanCodeInfo>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.ScanCode)
        self.assertEqual(msg.event, 'scancode')
        self.assertEqual(msg.code_type, 'barcode')
        self.assertEqual(msg.data, 'EAN_13,6911988011633')

    def test_parse_scancode_waitmsg(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512923818</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>scancode_waitmsg</Event>
        <EventKey>MenuID3</EventKey>
        <ScanCodeInfo>
            <ScanType>qrcode</ScanType>
            <ScanResult>http://www.baidu.com</ScanResult>
        </ScanCodeInfo>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.ScanCode)

    def test_parse_pic_sysphoto(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512923909</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>pic_sysphoto</Event>
        <EventKey>MenuID4</EventKey>
        <SendPicsInfo>
            <Count>1</Count>
            <PicList>
                <item>
                    <PicMd5Sum>8e7794e445a2096b9eb18123b93d37af</PicMd5Sum>
                </item>
            </PicList>
        </SendPicsInfo>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Pic)
        self.assertEqual(msg.event, 'pic')
        self.assertEqual(msg.image_md5_list, ['8e7794e445a2096b9eb18123b93d37af'])

    def test_parse_pic_weixin(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512924590</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>pic_weixin</Event>
        <EventKey>MenuID5</EventKey>
        <SendPicsInfo>
            <Count>2</Count>
            <PicList>
                <item>
                    <PicMd5Sum>86fc61ad01b03213fa07bcfb14fc479b</PicMd5Sum>
                </item>
            </PicList>
            <PicList>
                <item>
                    <PicMd5Sum>dd5e1aa0aad272c1be1224a3f7c2665a</PicMd5Sum>
                </item>
            </PicList>
        </SendPicsInfo>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.Pic)
        self.assertEqual(msg.image_md5_list, ['86fc61ad01b03213fa07bcfb14fc479b', 'dd5e1aa0aad272c1be1224a3f7c2665a'])

    def test_parse_location_select(self):
        element = XML("""<xml>
        <ToUserName>ww100000a5f2191</ToUserName>
        <FromUserName>wangn48</FromUserName>
        <CreateTime>1512925295</CreateTime>
        <MsgType>event</MsgType>
        <AgentID>9</AgentID>
        <Event>location_select</Event>
        <EventKey>MenuID7</EventKey>
        <SendLocationInfo>
            <Location_X>23.066633</Location_X>
            <Location_Y>113.377037</Location_Y>
            <Scale>16</Scale>
            <Label>Luo Xuan Da Dao, Panyu Qu, Guangzhou Shi, Guangdong Sheng, China</Label>
            <Poiname />
        </SendLocationInfo>
        </xml>""")
        msg = message.parse(element)
        self.assertIsInstance(msg, message.LocationSelect)
        self.assertEqual(msg.event, 'location_select')
        self.assertEqual(msg.latitude, 23.066633)
        self.assertEqual(msg.longitude, 113.377037)
        self.assertEqual(msg.scale, 16)
        self.assertEqual(msg.poi_name, '')

    def test_message_equal_and_hash(self):
        el1 = XML("""<xml>
            <ToUserName><![CDATA[toUser]]></ToUserName>
            <FromUserName><![CDATA[fromUser]]></FromUserName> 
            <CreateTime>1348831860</CreateTime>
            <MsgType><![CDATA[text]]></MsgType>
            <Content><![CDATA[this is a test]]></Content>
            <MsgId>1234567890123456</MsgId>
            <AgentID>1</AgentID>
        </xml>""")
        el2 = XML("""<xml>
           <ToUserName><![CDATA[toUser]]></ToUserName>
           <FromUserName><![CDATA[fromUser]]></FromUserName> 
           <CreateTime>1388888888</CreateTime>
           <MsgType><![CDATA[text]]></MsgType>
           <Content><![CDATA[this is a test]]></Content>
           <MsgId>1234567890123456</MsgId>
           <AgentID>1</AgentID>
        </xml>""")
        msg1 = message.parse(el1)
        msg2 = message.parse(el2)
        self.assertEqual(msg1, msg2)
        self.assertEqual(hash(msg1), hash(msg2))
        # Inconsistent MsgId
        el3 = XML("""<xml>
           <ToUserName><![CDATA[toUser]]></ToUserName>
           <FromUserName><![CDATA[fromUser]]></FromUserName> 
           <CreateTime>1348831860</CreateTime>
           <MsgType><![CDATA[text]]></MsgType>
           <Content><![CDATA[this is a test]]></Content>
           <MsgId>1234567890123457</MsgId>
           <AgentID>1</AgentID>
        </xml>""")
        msg3 = message.parse(el3)
        self.assertNotEqual(msg1, msg3)
        # Inconsistent MsgType
        el4 = XML("""<xml>
            <ToUserName><![CDATA[toUser]]></ToUserName>
            <FromUserName><![CDATA[UserID]]></FromUserName>
            <CreateTime>1348831860</CreateTime>
            <MsgType><![CDATA[event]]></MsgType>
            <Event><![CDATA[subscribe]]></Event>
            <AgentID>1</AgentID>
        </xml>""")
        msg4 = message.parse(el4)
        self.assertNotEqual(msg1, msg4)

    def test_event_equal_and_hash(self):
        el1 = XML("""<xml>
            <ToUserName><![CDATA[toUser]]></ToUserName>
            <FromUserName><![CDATA[UserID]]></FromUserName>
            <CreateTime>1348831860</CreateTime>
            <MsgType><![CDATA[event]]></MsgType>
            <Event><![CDATA[subscribe]]></Event>
            <AgentID>1</AgentID>
        </xml>""")
        el2 = XML("""<xml>
            <ToUserName><![CDATA[toUser]]></ToUserName>
            <FromUserName><![CDATA[UserID]]></FromUserName>
            <CreateTime>1348831860</CreateTime>
            <MsgType><![CDATA[event]]></MsgType>
            <Event><![CDATA[subscribe]]></Event>
            <AgentID>1</AgentID>
        </xml>""")
        msg1 = message.parse(el1)
        msg2 = message.parse(el2)
        self.assertEqual(msg1, msg2)
        self.assertEqual(hash(msg1), hash(msg2))
        # Inconsistent CreateTime
        el3 = XML("""<xml>
            <ToUserName><![CDATA[toUser]]></ToUserName>
            <FromUserName><![CDATA[UserID]]></FromUserName>
            <CreateTime>1348831861</CreateTime>
            <MsgType><![CDATA[event]]></MsgType>
            <Event><![CDATA[subscribe]]></Event>
            <AgentID>1</AgentID>
        </xml>""")

        msg3 = message.parse(el3)
        self.assertNotEqual(msg1, msg3)
        # Inconsistent Event
        el4 = XML("""<xml>
            <ToUserName><![CDATA[toUser]]></ToUserName>
            <FromUserName><![CDATA[UserID]]></FromUserName>
            <CreateTime>1348831860</CreateTime>
            <MsgType><![CDATA[event]]></MsgType>
            <Event><![CDATA[enter_agent]]></Event>
            <EventKey><![CDATA[]]></EventKey>
            <AgentID>1</AgentID>
        </xml>""")
        msg4 = message.parse(el4)
        self.assertNotEqual(msg1, msg4)


class TestResponse(TestCase):
    @classmethod
    def setUpClass(cls):
        element = XML("""<xml>
            <ToUserName><![CDATA[ww100000a5f2191]]></ToUserName>
            <FromUserName><![CDATA[wangn48]]></FromUserName>
            <CreateTime>1348831860</CreateTime>
            <MsgType><![CDATA[event]]></MsgType>
            <Event><![CDATA[subscribe]]></Event>
            <AgentID>1</AgentID>
        </xml>""")
        cls.msg = message.parse(element)

    def test_escape_cdata_close_tag(self):
        data = "string includes ']]>'"
        escaped = message._escape_cdata(data)
        self.assertEqual("string includes ']]]]><![CDATA[>'", escaped)

    def test_text_response(self):
        resp = message.TextResponse(self.msg, "Response: ]]>")
        el = XML(resp.to_xml())
        self.assertEqual('wangn48', el.findtext("ToUserName"))
        self.assertEqual('ww100000a5f2191', el.findtext("FromUserName"))
        self.assertEqual('1348831860', el.findtext("CreateTime"))
        self.assertEqual('text', el.findtext("MsgType"))
        self.assertEqual('Response: ]]>', el.findtext("Content"))

    def test_image_response(self):
        resp = message.ImageResponse(self.msg, "1HmqUWQ3giuiaE95L-siaFY541--l6GCWM9N8YarBhTs")
        el = XML(resp.to_xml())
        self.assertEqual('image', el.findtext("MsgType"))
        self.assertEqual('1HmqUWQ3giuiaE95L-siaFY541--l6GCWM9N8YarBhTs', el.findtext("Image/MediaId"))

    def test_voice_response(self):
        resp = message.VoiceResponse(self.msg, "MEDIA_ID")
        el = XML(resp.to_xml())
        self.assertEqual('voice', el.findtext("MsgType"))
        self.assertEqual('MEDIA_ID', el.findtext("Voice/MediaId"))

    def test_video_response(self):
        resp = message.VideoResponse(self.msg, "MEDIA_ID", "<[[Title]]>", "<[[Description]]>")
        el = XML(resp.to_xml())
        self.assertEqual('video', el.findtext("MsgType"))
        self.assertEqual('MEDIA_ID', el.findtext("Video/MediaId"))
        self.assertEqual('<[[Title]]>', el.findtext("Video/Title"))
        self.assertEqual('<[[Description]]>', el.findtext("Video/Description"))

    def test_news_response(self):
        item = {
            "title": "]]>",
            "description": "]]>",
            "pic_url": "http://example.com/example.png",
            "url": "http://example.com"
        }
        resp = message.NewsResponse(self.msg, [item])
        el = XML(resp.to_xml())
        self.assertEqual('news', el.findtext("MsgType")),
        self.assertEqual(']]>', el.findtext("Articles/item/Title"))
        self.assertEqual(']]>', el.findtext("Articles/item/Description"))
        self.assertEqual('http://example.com/example.png', el.findtext("Articles/item/PicUrl"))
        self.assertEqual('http://example.com', el.findtext("Articles/item/Url"))
