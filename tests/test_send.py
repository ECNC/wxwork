from unittest import TestCase
from unittest.mock import patch

from tests.api_test_helper import InvokeMock
from wxwork.config import Config
from wxwork.send import Send


@patch('wxwork.api.API._invoke', new_callable=InvokeMock)
class TestSend(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.conf = Config(corp_id="corp_id", secret="secret", agent_id=1)
        cls.send = Send(cls.conf)

    def test_constructor(self, _):
        s = Send(self.conf, agent_id=0)
        self.assertEqual(0, s.agent_id)
        self.assertEqual(1, self.send.agent_id)

    def test__send_process_receiver(self, mock):
        self.send._send("text", {}, None, "user", 1, ["1"])
        self.assertEqual("user", mock.json.touser)
        self.assertEqual("1", mock.json.toparty)
        self.assertEqual("1", mock.json.totag)

        self.send._send("text", {}, None, ["user1", "user2"], [1, 3], ["1", '2'])
        self.assertEqual("user1|user2", mock.json.touser)
        self.assertEqual("1|3", mock.json.toparty)
        self.assertEqual("1|2", mock.json.totag)

    def test__send_safe(self, mock):
        self.send._send("text", {}, False, 'user')
        self.assertNotIn("safe", mock.json)

        self.send._send("text", {}, True, 'user')
        self.assertEqual(1, mock.json.safe)
