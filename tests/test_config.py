from io import StringIO
from os import path
from unittest import TestCase

from wxwork.api import AccessTokenManager
from wxwork.config import Config
from wxwork.errors import ConfigError


class TestConfig(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.config_path = path.normpath(
            path.join(
                path.dirname(path.abspath(__file__)),
                '../example.conf'
            )
        )

    def test_config_from_fp(self):
        fp = StringIO(
            "[DEFAULT]\n"
            "CorpID = ww100000a5f2191\n"
            "Secret = keep_this_secret\n"
            "[test]\n"
            "Token = auj0VvlBr\n"
            "EncodingAESKey = 5SOzKmZFTGknmu4GRpP3Hk6pr41WiC1gfQ1WlGt67kj\n"
            "AgentID = 0\n"
        )
        conf = Config(file=fp, section="test")
        fp.close()

        self.assertEqual(conf.corp_id, 'ww100000a5f2191')
        self.assertEqual(conf.token, 'auj0VvlBr')
        self.assertEqual(conf.agent_id, 0)

    def test_config_from_path(self):
        conf = Config(file=self.config_path, section="example")
        self.assertEqual(conf.secret, 'keep_this_secret')

    def test_fallback_default_section(self):
        conf = Config(file=self.config_path)
        self.assertEqual(conf.secret, 'secret_of_Administrative_Group(Optional)')

    def test_nonexistent_section(self):
        with self.assertRaises(ConfigError):
            Config(file=self.config_path, section="bar")

    def test_default_config(self):
        with self.assertRaises(ConfigError):
            Config.get_default()

        Config(corp_id="CorpID", is_default=True)
        self.assertEqual(Config.get_default().corp_id, 'CorpID')

    def test_initial_token_maintainer(self):
        fp = StringIO(
            "[DEFAULT]\n"
            "CorpID = ww100000a5f2191\n"
            "Token = auj0VvlBr\n"
            "EncodingAESKey = 5SOzKmZFTGknmu4GRpP3Hk6pr41WiC1gfQ1WlGt67kj\n"
        )
        conf = Config(file=fp)
        fp.close()

        with self.assertRaises(ConfigError):
            _ = conf.access_token_manager

        conf = Config(file=self.config_path)
        self.assertIsInstance(conf.access_token_manager, AccessTokenManager)

    def test_get_agent_id(self):
        conf = Config(corp_id="CorpID", agent_id=0)
        self.assertEqual(0, conf.agent_id)
        conf = Config(corp_id="CorpID", agent_id=1)
        self.assertEqual(1, conf.agent_id)
        conf = Config(corp_id="CorpID")
        with self.assertRaises(ConfigError):
            _ = conf.agent_id

    def test_get_callback_url(self):
        conf = Config(corp_id="CorpID", callback_url="https://example.com")
        self.assertEqual("https://example.com", conf.callback_url)
        conf = Config(corp_id="CorpID")
        with self.assertRaises(ConfigError):
            _ = conf.callback_url

    def test_check_encoded_aes_key(self):
        with self.assertRaises(ConfigError):
            Config(corp_id="CorpID", token="Token", encoded_aes_key="InvalidKey")

    def test_get_callback_param(self):
        conf = Config(corp_id="CorpID", token="Token")
        with self.assertRaises(ConfigError):
            conf.get_callback_param()

        conf = Config(corp_id="CorpID", token="Token", encoded_aes_key="0" * 43)
        self.assertEqual(("CorpID", "Token", "0" * 43), conf.get_callback_param())
