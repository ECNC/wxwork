from unittest import TestCase
from urllib.parse import parse_qs, urlsplit

from wxwork.config import Config
from wxwork.auth import Auth


class TestAuth(TestCase):

    @classmethod
    def setUpClass(cls):
        cls.auth = Auth(Config(corp_id="CorpID", secret="Secret", agent_id=0))

    def test_get_oauth_url(self):
        url = self.auth.get_oauth_login_url(
            "https://example.com/callback", "state", Auth.OAUTH_SCOPE.BASIC
        )
        parse_result = urlsplit(url)
        query = parse_qs(parse_result.query)
        self.assertEqual("CorpID", query["appid"][0])
        self.assertEqual("https://example.com/callback", query["redirect_uri"][0])
        self.assertEqual("code", query["response_type"][0])
        self.assertEqual(Auth.OAUTH_SCOPE.BASIC.value, query["scope"][0])
        self.assertEqual("0", query["agentid"][0])
        self.assertEqual("state", query["state"][0])
        self.assertEqual("wechat_redirect", parse_result.fragment)

    def test_get_qr_auth_url(self):
        url = self.auth.get_qr_auth_login_url("https://example.com/callback", "state")
        parse_result = urlsplit(url)
        query = parse_qs(parse_result.query)
        self.assertEqual("CorpID", query["appid"][0])
        self.assertEqual("0", query["agentid"][0])
        self.assertEqual("https://example.com/callback", query["redirect_uri"][0])
        self.assertEqual("state", query["state"][0])

    def test_get_legacy_qr_auth_url(self):
        url = self.auth.get_legacy_qr_auth_login_url(
            "https://example.com/callback", "state", "member"
        )
        parse_result = urlsplit(url)
        query = parse_qs(parse_result.query)
        self.assertEqual("CorpID", query["corp_id"][0])
        self.assertEqual("https://example.com/callback", query["redirect_uri"][0])
        self.assertEqual("state", query["state"][0])
        self.assertEqual("member", query["usertype"][0])
