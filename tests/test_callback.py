from unittest import TestCase
from unittest.mock import patch
from xml.etree.ElementTree import Element

from wxwork import callback
from wxwork.config import Config
from wxwork.errors import SignatureError


class TestCallback(TestCase):
    @classmethod
    def setUpClass(cls):
        config = Config(corp_id='ww100000a5f2191', token='auj0VvlBr',
                        encoded_aes_key='5SOzKmZFTGknmu4GRpP3Hk6pr41WiC1gfQ1WlGt67kj')
        cls.callback = callback.Callback(config)

    @patch('wxwork.callback.parse')
    def test_parse(self, mock_parse):
        data = b"""<xml>
        <ToUserName><![CDATA[ww100000a5f2191]]></ToUserName>
        <Encrypt><![CDATA[QjFDtTiHCYtiTrWvaW+5edlfZ6ub0MIf3DPdDXIQoeYkKHnvuBi15Mo37iboUyK0/L1rsg2+xjBUmESOSQIYYYAQWN/Y1S8lhymLDokGNRiVgJbpmZ4Tad0XEIXVSDnpkYUOUBHL6ebnjijdoqeirkCRDzREt+cLiIFNfUYt4DSbIxsn3hxHImRguuLBBgH7kdZhOpm6bORydlT87Jfap1OXoe0QizBN4jcngLrhRmzIo5UvKtJZbT3zsu4odMUCEKPDoMej3XZHGq2P0nMK+W3Zwm0HqTmNdhN7MUJxNkS7AfF++3IvkPj4WQacWFr3xhzIqewtzfIm7nAAQABfUiUChdnbxtAwso7dkFN0N6of+xPn+c0FYMZOnhr0AC0sWd1x4PCleQuLu0Hv4KPtzn0oZhmP0ZxCZdV/eM/Lek0=]]></Encrypt>
        <AgentID><![CDATA[9]]></AgentID>
        </xml>"""
        signature = '760b8d6a539cf5d7a6a6d17c8958f40bbde88598'
        timestamp = '1514012741'
        nonce = '416631029'
        self.callback.parse(data, signature, timestamp, nonce)

        self.assertTrue(mock_parse.called)
        self.assertIsInstance(mock_parse.call_args[0][0], Element)

    def test_verify_url(self):
        signature = '9c8d1d85296080ae0f172d3051bc528d50350d0f'
        timestamp = '1513884170'
        nonce = '2091306515'
        echostr = '8aeHuUYF6idE4tzQ02TWI8kxCElj4oMXvnCwTv1uKJnxLbK65wyGPJlwcUqcu4iSXGHeT5yHzDqE1jZeHKSaUg=='
        echo = self.callback.verify_url(signature, timestamp, nonce, echostr)
        self.assertEqual(echo, b'4576930535313423054')
        with self.assertRaises(SignatureError):
            self.callback.verify_url('foobar', timestamp, nonce, echostr)
