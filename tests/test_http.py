import json
from collections import OrderedDict
from unittest import TestCase
from unittest.mock import patch

import urllib3.exceptions

from wxwork import http, errors


class TestHttp(TestCase):
    @patch.object(http, 'getrandbits', return_value=0)
    def test_encode_multipart(self, _):
        filename = "Unicode 测试.txt"
        data = b'\x00\x01\x02'
        mime = "application/octet-stream"
        body, content_type = http.encode_multipart(filename, data, mime)
        self.assertEqual((
            b'------WXWorkFormBoundary00000000\r\n'
            b'Content-Disposition: form-data; name="media"; filename="Unicode%20%E6%B5%8B%E8%AF%95.txt"\r\n'
            b'Content-Type: application/octet-stream\r\n'
            b'\r\n'
            b'\x00\x01\x02\r\n'
            b'------WXWorkFormBoundary00000000--\r\n'), body
        )
        self.assertEqual(
            "multipart/form-data; boundary=----WXWorkFormBoundary00000000",
            content_type
        )

    def test_prepare_request_json(self):
        data = {"name": "张三"}
        body, headers = http.prepare_request(data, None)
        self.assertEqual(data, json.loads(body.decode()))
        self.assertEqual("keep-alive", headers["Connection"])
        self.assertEqual("application/json", headers["Content-Type"])

    @patch.object(http, 'encode_multipart', return_value=(b'body', 'ContentType'))
    def test_prepare_request_file(self, mock_method):
        file = ("test.txt", b"", "text/plain")
        body, headers = http.prepare_request(None, file)
        mock_method.assert_called_once_with(*file)
        self.assertEqual(b"body", body)
        self.assertEqual("keep-alive", headers["Connection"])
        self.assertEqual("ContentType", headers["Content-Type"])

    @patch.object(http.POOL, 'urlopen')
    def test_request_post(self, mock_urlopen):
        mock_urlopen.return_value.status = 200
        mock_urlopen.return_value.data = b'{}'
        method = "POST"
        path = "test"
        query = OrderedDict({"access_token": "abc123"})
        body = b"{}"
        headers = {"X-Header": "bar"}
        ret = http.request(path, query, body, headers)
        mock_urlopen.assert_called_once_with(
            method, "/cgi-bin/test?access_token=abc123",
            body, headers, assert_same_host=False
        )
        self.assertEqual({}, ret)

        query["agent_id"] = "agent_id"
        http.request(path, query, body, headers)
        mock_urlopen.assert_called_with(
            method, "/cgi-bin/test?access_token=abc123&agent_id=agent_id",
            body, headers, assert_same_host=False
        )

    @patch.object(http.POOL, 'urlopen')
    def test_request_get(self, mock_urlopen):
        mock_urlopen.return_value.status = 200
        mock_urlopen.return_value.data = b'{}'
        method = "GET"
        path = "test"
        query = OrderedDict({"access_token": "abc123"})

        ret = http.request(path, query, None, None)
        mock_urlopen.assert_called_once_with(
            method, "/cgi-bin/test?access_token=abc123",
            None, {"Connection": "keep-alive"}, assert_same_host=False
        )
        self.assertEqual({}, ret)

    @patch.object(http.POOL, 'urlopen')
    def test_request_exceptions(self, mock_urlopen):
        path = "error"
        query = {"foo": "bar"}
        request_args = (path, query, None, None)

        # Exceptions during request
        mock_urlopen.side_effect = urllib3.exceptions.HTTPError
        with self.assertRaises(errors.HTTPError):
            http.request(*request_args)

        # Unexpected status code
        mock_urlopen.side_effect = None
        mock_urlopen.return_value.status = 500
        with self.assertRaises(errors.HTTPError):
            http.request(*request_args)

        # Illegal response
        mock_urlopen.return_value.status = 200
        mock_urlopen.return_value.data = b''
        with self.assertRaises(errors.HTTPError):
            http.request(*request_args)

        # Illegal response(but legal JSON)
        mock_urlopen.return_value.data = b'1'
        with self.assertRaises(errors.HTTPError):
            http.request(*request_args)
