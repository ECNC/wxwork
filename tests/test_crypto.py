from base64 import b64encode
from unittest import TestCase
from unittest.mock import patch
from xml.etree.ElementTree import XML

from wxwork import crypto


def int2byte(i):
    return i.to_bytes(1, 'big')


class TestCrypto(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.corp_id = 'ww100000a5f2191'
        cls.crypto = crypto.Wrapper(
            cls.corp_id, 'auj0VvlBr',
            '5SOzKmZFTGknmu4GRpP3Hk6pr41WiC1gfQ1WlGt67kj'
        )

    def test_padding(self):
        block_sizes = [8, 16, 32, 64]
        for block_size in block_sizes:
            with self.subTest(block_size=block_size):
                data = b''
                padded = crypto.Cryptography.pad(data, block_size)
                self.assertEqual(int2byte(block_size) * block_size, padded)
                unpadded = crypto.Cryptography.unpad(padded)
                self.assertEqual(data, unpadded)

                data = int2byte(block_size) * block_size
                padded = crypto.Cryptography.pad(data, block_size)
                self.assertEqual(data + int2byte(block_size) * block_size, padded)
                unpadded = crypto.Cryptography.unpad(padded)
                self.assertEqual(data, unpadded)

                data = b'\x00' * 127
                padded = crypto.Cryptography.pad(data, block_size)
                self.assertEqual(data + b'\x01', padded)
                unpadded = crypto.Cryptography.unpad(padded)
                self.assertEqual(data, unpadded)

                data = b"\x00" * 129
                padded = crypto.Cryptography.pad(data, block_size)
                self.assertEqual(data + int2byte(block_size - 1) * (block_size - 1), padded)
                unpadded = crypto.Cryptography.unpad(padded)
                self.assertEqual(data, unpadded)

    def test_encrypt_and_decrypt(self):
        data = b'ABC123'
        cipher = self.crypto.cryptor.encrypt(data)
        self.assertEqual(cipher, b'1YB+g7LAP8Eoc4dWYr1pTeyZ5NkYVpAtc8uxlyBpiuI=')
        decrypted = self.crypto.cryptor.decrypt(cipher)
        self.assertEqual(data, decrypted)

    def test_sign(self):
        signature = self.crypto.sign(b'1', '2', '3')
        # SHA1('123auj0VvlBr')
        self.assertEqual(signature, '0a811dd3af5a4d45b050ac4c9722149cdd2ff99f')

    @patch.object(crypto, 'urandom', side_effect=lambda x: b'\x00' * x)
    @patch.object(crypto, 'getrandbits', return_value=0)
    @patch.object(crypto.Cryptography, 'encrypt', side_effect=lambda x: b64encode(x))
    def test_encapsulate(self, mock_encrypt, mock_getrandbits, mock_urandom):
        data = 'ABC123'
        timestamp = 1234567890
        encapsulated = XML(self.crypto.encapsulate(data, timestamp))
        self.assertEqual(
            b64encode(
                bytes(16) + len(data).to_bytes(4, 'big') + data.encode() + self.corp_id.encode()
            ),
            encapsulated.findtext('Encrypt').encode()
        )
        self.assertTrue(encapsulated.findtext('MsgSignature'))
        self.assertEqual(timestamp, int(encapsulated.findtext('TimeStamp')))
        self.assertEqual('0', encapsulated.findtext('Nonce'))

    def test_extract(self):
        cipher = '8e7qd2RCpnD7QKrt2OYdXxpmgfMmOsnnP9juGnPKpIt+2ZNjOgZVAHk1ts4FToNH'
        signature = "a093220a85e06b48d3b3caffb1ae5fe7c04800f9"
        timestamp = "1234567890"
        nonce = "0"
        plaintext = self.crypto.extract(cipher, signature, timestamp, nonce)
        self.assertEqual(b'ABC123', plaintext)
