from unittest import TestCase
from unittest.mock import patch

from tests.api_test_helper import InvokeMock
from wxwork.config import Config
from wxwork.contacts import Contacts


@patch('wxwork.api.API._invoke', new_callable=InvokeMock)
class TestContacts(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.conf = Config(corp_id='CorpID', secret='Secret',
                          token='Token', encoded_aes_key='0' * 43,
                          callback_url='https://example.com')
        cls.contacts = Contacts(cls.conf)

    def test_async_update_callback(self, mock):
        self.contacts.async_update(Contacts.UpdateMode.INCREMENTAL, 'MediaID')
        self.assertNotIn('callback', mock.json)

        self.contacts.async_update(Contacts.UpdateMode.INCREMENTAL, 'MediaID',
                                   callback=True)
        self.assertEqual(
            {
                'url': self.conf.callback_url,
                'token': self.conf.token,
                'encodingaeskey': self.conf.encoded_aes_key
            },
            mock.json.callback
        )

    def test_add_user_set_dept(self, mock):
        self.contacts.add_user('UserID', 'Name', department=1,
                               email='foo@bar.com')
        self.assertEqual([1], list(mock.json.department))

        self.contacts.add_user('UserID', 'Name', department=[1, 2],
                               email='foo@bar.com')
        self.assertEqual([1, 2], list(mock.json.department))

    def test_add_user_set_order(self, mock):
        self.contacts.add_user('UserID', 'Name', order=1, email='foo@bar.com')
        self.assertEqual([1], list(mock.json.order))

        self.contacts.add_user('UserID', 'Name', department=[1, 2],
                               order=(0, 0), email='foo@bar.com')
        self.assertEqual([0, 0], list(mock.json.order))

    def test_update_user_dept(self, mock):
        self.contacts.update_user('UserID', department=1)
        self.assertEqual([1], list(mock.json.department))

        self.contacts.update_user('UserID', department=[1, 2])
        self.assertEqual([1, 2], list(mock.json.department))

    def test_update_user_order(self, mock):
        self.contacts.update_user('UserID', department=1, order=0)
        self.assertEqual([0], list(mock.json.order))

        self.contacts.update_user('UserID', department=[1, 2], order=(0, 0))
        self.assertEqual([0, 0], list(mock.json.order))

    def test_add_tag_members(self, mock):
        self.contacts.add_tag_members(0, 'UserID', 1)
        self.assertEqual(['UserID'], list(mock.json.userlist))
        self.assertEqual([1], list(mock.json.partylist))

        self.contacts.add_tag_members(0, ['UserID'], [1, 2])
        self.assertEqual(['UserID'], mock.json.userlist)
        self.assertEqual([1, 2], mock.json.partylist)

    def test_del_tag_members(self, mock):
        self.contacts.del_tag_members(0, 'UserID', 1)
        self.assertEqual(['UserID'], list(mock.json.userlist))
        self.assertEqual([1], list(mock.json.partylist))

        self.contacts.del_tag_members(0, ['UserID'], [1, 2])
        self.assertEqual(['UserID'], mock.json.userlist)
        self.assertEqual([1, 2], mock.json.partylist)
