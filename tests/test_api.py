from unittest import TestCase
from unittest.mock import patch

from wxwork import config, api, http, errors


@patch.object(api.AccessTokenManager, 'get_access_token', return_value="at")
@patch.object(http, 'request')
class TestAPI(TestCase):
    API_OK = {"errcode": 0, "errmsg": "ok"}
    API_TOKEN_ERR = {"errcode": 40014, "errmsg": "invalid access_token"}
    API_GENERIC_ERR = {"errcode": 40004, "errmsg": ""}

    @classmethod
    def setUpClass(cls):
        conf = config.Config(corp_id="corp_id", secret="secret")
        cls.api = api.API(conf)

    def test_invoke_with_json(self, mock_request, mock_at):
        mock_request.return_value = self.API_OK

        api_path = "foo/bar"
        json = {"foo": "bar"}

        result = self.api._invoke(api_path, json, key="value")
        mock_request.assert_called_once_with(
            api_path,
            {"key": "value", "access_token": "at"},
            *http.prepare_request(json, None)
        )
        mock_at.assert_called_once_with()
        self.assertEqual(self.API_OK, result)

    @patch.object(http, 'getrandbits', return_value=0)
    def test_invoke_with_file(self, _, mock_request, mock_at):
        mock_request.return_value = self.API_OK

        api_path = "foo/bar"
        file = ('test.file', b'\x00', 'application/octet-stream')

        result = self.api._invoke(api_path, file=file, key="value")
        mock_request.assert_called_once_with(
            api_path,
            {"key": "value", "access_token": "at"},
            *http.prepare_request(None, file)
        )
        mock_at.assert_called_once_with()
        self.assertEqual(self.API_OK, result)

    def test_invoke_without_body(self, mock_request, mock_at):
        mock_request.return_value = self.API_OK

        api_path = "foo/bar"

        result = self.api._invoke(api_path)
        mock_request.assert_called_once_with(
            api_path,
            {"access_token": "at"},
            None, None
        )
        mock_at.assert_called_once_with()
        self.assertEqual(self.API_OK, result)

    def test_access_token_error(self, mock_request, mock_at):
        mock_request.side_effect = [self.API_TOKEN_ERR, self.API_OK]
        result = self.api._invoke("foo/bar")
        mock_at.assert_called_with("at")
        self.assertEqual(2, mock_request.call_count)
        self.assertEqual(2, mock_at.call_count)
        self.assertEqual(self.API_OK, result)

    def test_access_token_error_twice(self, mock_request, mock_at):
        mock_request.return_value = self.API_TOKEN_ERR
        with self.assertRaises(errors.APIError):
            self.api._invoke("foo/bar")
        self.assertEqual(2, mock_at.call_count)
        self.assertEqual(2, mock_request.call_count)

    def test_generic_error(self, mock_request, mock_at):
        mock_request.return_value = self.API_GENERIC_ERR
        with self.assertRaises(errors.APIError):
            self.api._invoke("foo/bar")
        mock_at.assert_called_once_with()
        self.assertEqual(1, mock_request.call_count)
