from os import path

from setuptools import setup

with open(path.join(path.abspath(path.dirname(__file__)), 'README.rst'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='wxwork',

    version='0.3.1',

    description='A library for accessing WeChat Work APIs',
    long_description=long_description,

    url='https://bitbucket.org/ECNC/wechat-py3',

    author='Ning Wang',
    author_email='nagisa@ecnc.xyz',

    license='Apache-2.0',

    classifiers=[
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',
        'Topic :: Software Development :: Libraries',

        'License :: OSI Approved :: Apache Software License',

        'Programming Language :: Python :: 3 :: Only',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7'
    ],
    python_requires='>=3.4',

    keywords='wechat work enterprise api',

    packages=['wxwork'],

    install_requires=['cryptography', 'urllib3']
)
