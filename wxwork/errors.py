class WXWorkError(Exception):
    pass


class ConfigError(WXWorkError):
    pass


class APIError(WXWorkError):
    def __init__(self, errcode, errmsg, **kwargs):
        message = "%s: %s" % (errcode, errmsg)
        if kwargs:
            additional = ", ".join(["%s: %s" % i for i in kwargs.items()])
            message = "%s [%s]" % (message, additional)
        super().__init__(message)
        self.errcode = errcode
        self.errmsg = errmsg


class HTTPError(WXWorkError):
    pass


class SignatureError(WXWorkError):
    pass


class ArgumentError(WXWorkError, ValueError):
    pass
