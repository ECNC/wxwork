from xml.etree.ElementTree import XML

from .config import Config
from .crypto import Wrapper
from .message import (parse, UserAction,
                      TextResponse, ImageResponse, VoiceResponse, VideoResponse, NewsResponse)


class Callback:

    def __init__(self, conf=None):
        if not conf:
            conf = Config.get_default()
        elif not isinstance(conf, Config):
            raise TypeError("Argument must be an instance of Config")
        self._wrapper = Wrapper(*conf.get_callback_param())

    def _encapsulate(self, response):
        return self._wrapper.encapsulate(response.to_xml(), response.create_time)

    def parse(self, data, msg_signature, timestamp, nonce):
        """Parse WeChat callback data

        :param bytes data: Raw WeChat callback POST data
        :type msg_signature: str
        :type timestamp: str
        :type nonce: str
        :return: Instance of specific message type
        """
        plain_xml = self._wrapper.extract(XML(data).findtext('Encrypt'),
                                          msg_signature, timestamp, nonce)
        return parse(XML(plain_xml))

    def reply_text(self, message, content):
        """

        :type message: UserAction
        :param str content: Text content
        """
        return self._encapsulate(TextResponse(message, content))

    def reply_image(self, message, media_id):
        """

        :type message: UserAction
        :param str media_id: MediaID of image
        """
        return self._encapsulate(ImageResponse(message, media_id))

    def reply_voice(self, message, media_id):
        """

        :type message: UserAction
        :param str media_id: MediaID of voice
        """
        return self._encapsulate(VoiceResponse(message, media_id))

    def reply_video(self, message, media_id, title, description):
        """

        :type message: UserAction
        :param str media_id: MediaID of video
        :param str title: Video title
        :param str description: Video description
        """
        return self._encapsulate(VideoResponse(message, media_id, title, description))

    def reply_news(self, message, articles):
        """

        :type message: UserAction
        :param articles: Callback.gen_news
        :type articles: list[dict]
        """
        return self._encapsulate(NewsResponse(message, articles))

    def verify_url(self, msg_signature, timestamp, nonce, echostr):
        """Decrypt and return echostr to handle WeChat callback url challenge

        https://work.weixin.qq.com/api/doc#10514
        :type msg_signature: str
        :type timestamp: str
        :type nonce: str
        :type echostr: str
        :rtype: bytes
        """
        return self._wrapper.extract(echostr, msg_signature, timestamp, nonce)

    @staticmethod
    def gen_news(title, description, pic_url, url):
        return {"title": title, "description": description, "pic_url": pic_url, "url": url}
