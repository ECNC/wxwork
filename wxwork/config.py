import configparser
import io

from .errors import ConfigError


class Config:
    default = None

    def __init__(self, corp_id=None, secret=None, token=None, encoded_aes_key=None,
                 agent_id=None, callback_url=None,
                 file=None, section=None, is_default=False):
        """

        :param str corp_id: CorpID (required if you don't provide a config file)
        :param str secret: Secret
        :param str token: Token
        :param str encoded_aes_key: EncodingAESKey
        :param int agent_id: AgentID
        :param str callback_url: Agent's Callback URL
        :param file: Config file path or file opened in text mode
        :type file: str or os.PathLike or io.TextIOBase
        :param str section: Section name of config file
        :param bool is_default: Set as default config
        """
        if not corp_id:
            try:
                config = self._parse_config(file, section)
            except configparser.Error as e:
                raise ConfigError() from e

            corp_id = config.get("CorpID")
            if not corp_id:
                raise ConfigError("Required option 'CorpID' not found in config file")
            secret = config.get("Secret")
            encoded_aes_key = config.get("EncodingAESKey")
            token = config.get("Token")
            agent_id = config.getint("AgentID")
            callback_url = config.get("CallbackURL")

        if secret:
            from .api import AccessTokenManager
            self._access_token_manager = AccessTokenManager(corp_id, secret)
        else:
            self._access_token_manager = None
        if encoded_aes_key and token:
            if len(encoded_aes_key) != 43:
                raise ConfigError("Invalid EncodingAESKey")
            self._is_callback_able = True
        else:
            self._is_callback_able = False

        self.corp_id = corp_id
        self.secret = secret
        self.encoded_aes_key = encoded_aes_key
        self.token = token
        self._agent_id = agent_id
        self._callback_url = callback_url

        if is_default:
            Config.default = self

    @staticmethod
    def _parse_config(file, section):
        config = configparser.ConfigParser()
        if isinstance(file, io.IOBase):
            try:
                config.read_file(file)
            except TypeError:
                raise ConfigError("Stream must be opened in text mode")
        elif not config.read(file, 'utf-8'):
            raise ConfigError("No such file: '%s'" % file)

        if section:
            try:
                return config[section]
            except KeyError:
                raise ConfigError("Section '%s' not found" % section)
        elif "DEFAULT" in config:
            return config["DEFAULT"]
        else:
            raise ConfigError("Section not specified and no DEFAULT found")

    @classmethod
    def get_default(cls):
        if cls.default:
            return cls.default
        else:
            raise ConfigError("Configuration is not specified")

    @property
    def access_token_manager(self):
        if self._access_token_manager:
            return self._access_token_manager
        else:
            raise ConfigError("API is unavailable without Secret set")

    @property
    def agent_id(self):
        if self._agent_id is not None:
            return self._agent_id
        else:
            raise ConfigError("Option 'AgentID' not configured")

    @property
    def callback_url(self):
        if self._callback_url:
            return self._callback_url
        else:
            raise ConfigError("Option 'callback_url' is not set in Config")

    def get_callback_param(self):
        if self._is_callback_able:
            return self.corp_id, self.token, self.encoded_aes_key
        else:
            raise ConfigError("Callback is unavailable without EncodingAESKey and Token configured")
