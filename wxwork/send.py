from .api import API


class SendInterface(API):
    def _send(self, *args, **kwargs):
        """

        :type msg_type: str
        :type msg: dict[str, Any]
        :type safe: Optional[bool]
        """
        raise NotImplementedError

    def send_text(self, content, *args, safe=False, **kwargs):
        """

        :type content: str
        :type safe: bool
        """
        msg = {
            "content": content
        }
        return self._send("text", msg, safe, *args, **kwargs)

    def send_image(self, media_id, *args, safe=False, **kwargs):
        """

        :type media_id: str
        :type safe: bool
        """
        msg = {
            "media_id": media_id
        }
        return self._send("image", msg, safe, *args, **kwargs)

    def send_voice(self, media_id, *args, **kwargs):
        """

        :type media_id: str
        """
        msg = {
            "media_id": media_id
        }
        return self._send("voice", msg, None, *args, **kwargs)

    def send_video(self, media_id, title=None, description=None,
                   *args, safe=False, **kwargs):
        """

        :type media_id: str
        :type title: str
        :type description: str
        :type safe: bool
        """
        msg = {
            "media_id": media_id,
            "title": title,
            "description": description
        }
        return self._send("video", msg, safe, *args, **kwargs)

    def send_file(self, media_id, *args, safe=False, **kwargs):
        """

        :type media_id: str
        :type safe: bool
        """
        msg = {
            "media_id": media_id
        }
        return self._send("file", msg, safe, *args, **kwargs)

    def send_text_card(self, title, description, url, btn_text=None,
                       *args, **kwargs):
        """

        :type title: str
        :type description: str
        :type url: str
        :type btn_text: str
        """
        msg = {
            "title": title,
            "description": description,
            "url": url,
            "btntxt": btn_text
        }
        return self._send("textcard", msg, None, *args, **kwargs)

    def send_news(self, articles, *args, **kwargs):
        """

        :type articles: Union[Dict[str, str], Iterable[Dict[str, str]]]
        """
        msg = {
            "articles": [articles] if isinstance(articles, dict) else articles
        }
        return self._send("news", msg, None, *args, **kwargs)

    def send_mpnews(self, articles, *args, safe=False, **kwargs):
        """

        :type articles: Union[Dict[str, str], Iterable[Dict[str, str]]]
        :type safe: bool
        """
        msg = {
            "articles": [articles] if isinstance(articles, dict) else articles
        }
        return self._send("mpnews", msg, safe, *args, **kwargs)

    @staticmethod
    def gen_news(title, description=None, url=None, pic_url=None, btn_text=None):
        """

        :type title: str
        :type description: str
        :type url: str
        :type pic_url: str
        :type btn_text: str
        :rtype: dict[str, str]
        """
        return {
            "title": title,
            "description": description,
            "url": url,
            "picurl": pic_url,
            "btntxt": btn_text
        }

    @staticmethod
    def gen_mpnews(title, thumb_media_id, content=None, author=None,
                   content_source_url=None, digest=None):
        """

        :type title: str
        :type thumb_media_id: str
        :type content: str
        :type author: str
        :type content_source_url: str
        :type digest: str
        :rtype: dict[str, str]
        """
        return {
            "title": title,
            "thumb_media_id": thumb_media_id,
            "author": author,
            "content_source_url": content_source_url,
            "content": content,
            "digest": digest
        }


class Send(SendInterface):
    """Message sending

    https://work.weixin.qq.com/api/doc#10167
    """
    def __init__(self, config=None, agent_id=None):
        super().__init__(config)
        if agent_id is None:
            agent_id = self._conf.agent_id
        self.agent_id = agent_id

    def _send(self, msg_type, msg, safe, user=None, dept=None, tag=None):
        """

        :type user: Optional[Union[str, Iterable[str]]]
        :type dept: Optional[Union[int, Iterable[int]]]
        :type tag: Optional[Union[int, Iterable[int]]]
        :rtype: dict
        """
        if not user and not dept and not tag:
            raise TypeError("Missing 1 required argument: 'user' or 'dept' or 'tag'")
        if user and not isinstance(user, str):
            user = '|'.join(user)
        if dept:
            if isinstance(dept, int):
                dept = str(dept)
            elif not isinstance(dept, str):
                dept = '|'.join([str(i) for i in dept])
        if tag:
            if isinstance(tag, int):
                tag = str(tag)
            elif not isinstance(tag, str):
                tag = '|'.join([str(i) for i in tag])
        data = {
            "touser": user,
            "toparty": dept,
            "totag": tag,
            "agentid": self.agent_id,
            "msgtype": msg_type,
            msg_type: msg
        }
        if safe:
            data['safe'] = 1
        return self._invoke("message/send", data)
