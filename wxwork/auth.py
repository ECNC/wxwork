from enum import Enum
from urllib.parse import quote_plus

from .api import API


class OAuthScope(Enum):
    BASIC = 'snsapi_base'
    GENERAL = 'snsapi_userinfo'
    PRIVATE = 'snsapi_privateinfo'


class Auth(API):
    OAUTH_SCOPE = OAuthScope

    def __init__(self, config=None):
        super().__init__(config)

        self._oauth_login_url = (
            "https://open.weixin.qq.com/connect/oauth2/authorize?"
            "response_type=code&appid={}&agentid={}&"
            "scope=%s&redirect_uri=%s&state=%s#wechat_redirect"
        ).format(self._conf.corp_id, self._conf.agent_id)
        self._qr_auth_login_url = (
            "https://open.work.weixin.qq.com/wwopen/sso/qrConnect?"
            "appid={}&agentid={}&"
            "redirect_uri=%s&state=%s"
        ).format(self._conf.corp_id, self._conf.agent_id)
        self._legacy_qr_auth_login_url = (
            "https://qy.weixin.qq.com/cgi-bin/loginpage?"
            "corp_id={}&"
            "usertype=%s&redirect_uri=%s&state=%s"
        ).format(self._conf.corp_id)

    def get_oauth_login_url(self, redirect_uri, state, scope):
        """

        https://work.weixin.qq.com/api/doc#10028/获取code

        :type redirect_uri: str
        :type state: str
        :type scope: OAuthScope
        """
        url = self._oauth_login_url % (scope.value, quote_plus(redirect_uri), state)
        return url

    def get_qr_auth_login_url(self, redirect_uri, state):
        """

        https://work.weixin.qq.com/api/doc#10719/使用说明

        :type redirect_uri: str
        :type state: str
        """
        url = self._qr_auth_login_url % (quote_plus(redirect_uri), state)
        return url

    def get_legacy_qr_auth_login_url(self, redirect_uri, state, user_type):
        """

        http://qydev.weixin.qq.com/wiki/index.php?title=成员登录授权

        :type redirect_uri: str
        :type state: str
        :type user_type: str
        """
        url = self._legacy_qr_auth_login_url % (user_type, quote_plus(redirect_uri), state)
        return url

    def verify_auth_code(self, code):
        """

        https://work.weixin.qq.com/api/doc#10028/根据code获取成员信息

        :type code: str
        """
        return self._invoke("user/getuserinfo", code=code)

    def verify_legacy_qr_auth_code(self, auth_code):
        """

        https://work.weixin.qq.com/api/doc#10991/获取登录用户信息

        :type auth_code: str
        """
        return self._invoke("service/get_login_info", {"auth_code": auth_code})

    def get_oauth_user_details(self, user_ticket):
        """

        https://work.weixin.qq.com/api/doc#10028/使用user_ticket获取成员详情

        :type user_ticket: str
        """
        return self._invoke("user/getuserdetail", {"user_ticket": user_ticket})
