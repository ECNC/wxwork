from enum import Enum

from .api import API
from .errors import ArgumentError

EXT2MIME = {
    'amr': 'audio/AMR',
    'jpeg': 'image/jpeg',
    'jpg': 'image/jpeg',
    'mp4': 'video/mp4',
    'png': 'image/png'
}
BINARY_MIME = 'application/octet-stream'


class FileType(Enum):
    IMAGE = 'image'
    VOICE = 'voice'
    VIDEO = 'video'
    FILE = 'file'


EXT2FILE_TYPE = {
    'amr': FileType.VOICE,
    'jpeg': FileType.IMAGE,
    'jpg': FileType.IMAGE,
    'mp4': FileType.VIDEO,
    'png': FileType.IMAGE
}

MIN_SIZE = 6
MAX_SIZE = {
    FileType.IMAGE: 2097152,  # 2 MiB
    FileType.VOICE: 2097152,
    FileType.VIDEO: 10485760,  # 10 MiB
    FileType.FILE: 20971520  # 20 MiB
}


class Media(API):
    """Material files management

    https://work.weixin.qq.com/api/doc#10112
    """
    FileType = FileType

    @staticmethod
    def _handle_file(filename, file, file_type, mime):
        if hasattr(file, "read"):
            file = file.read()
        size = len(file)
        if size < MIN_SIZE:
            raise ArgumentError("File size must be greater than 5 bytes")
        if not file_type or not mime:
            parts = filename.rsplit('.', 1)
            extension = parts[1].lower() if len(parts) > 1 else None
            file_type = file_type or EXT2FILE_TYPE.get(extension, FileType.FILE)
            mime = EXT2MIME.get(extension, BINARY_MIME)

        if size > MAX_SIZE[file_type]:
            raise ArgumentError("File is too large")

        return file_type.value, (filename, file, mime)

    def upload_temporary(self, filename, file, file_type=None, mime=None):
        """Upload a temporary material file (valid for 3 days)

        :type filename: str
        :param file: bytes-like object or file-like object
        :param file_type: Material type in (image, voice, video, file)
        :type file_type: FileType or str
        :param str mime: MIME type
        :rtype: dict
        :return: {
            'type': '{file_type}',
            'media_id': '3dG3tvSr2pBCXjTxCKDu6v_ifPomGepul0zXP3qwRAF8',
            'created_at': '1537730672'
        }
        """
        if filename is None:
            raise TypeError("filename must be a string, not 'NoneType'")
        if len(filename) == 0:
            raise ArgumentError('filename must not be empty')
        if file_type is not None:
            file_type = FileType(file_type)
        file_type, file = self._handle_file(filename, file, file_type, mime)
        return self._invoke("media/upload", file=file, type=file_type)

    def upload_mpnews_image(self, filename, file, mime=None):
        """

        :type filename: str
        :param file: bytes-like object or file-like object
        :param str mime: MIME type
        :return: permanent image url
        :rtype: str
        """
        if not filename:
            raise ArgumentError('Invalid filename')
        _, file = self._handle_file(filename, file, FileType.IMAGE, mime)
        return self._invoke('media/uploadimg', file=file)['url']

    def get_temporary(self, media_id):
        return self._gen_url('media/get', media_id=media_id)

    def get_hq_voice(self, media_id):
        return self._gen_url('media/get/jssdk', media_id=media_id)
