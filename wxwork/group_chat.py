from .send import SendInterface


class GroupChat(SendInterface):
    """Group chat management and messaging

    https://work.weixin.qq.com/api/doc#13308
    """
    def __init__(self, config=None):
        super().__init__(config)

    def create(self, users, name=None, owner=None, chat_id=None):
        """

        https://work.weixin.qq.com/api/doc#13288

        :param list[str] users: 2 to 500 user_id
        :param str name: name of the group chat
        :param str owner: group owner's user_id
        :param str chat_id: unique ID of the group chat, 1 to 32 characters in 0-9A-Za-z
        :rtype: str
        :return: chat_id of created group chat
        """
        payload = {
            'chatid': chat_id,
            'name': name,
            'owner': owner,
            'userlist': users
        }
        return self._invoke('appchat/create', payload)['chatid']

    def update(self, chat_id, name=None, owner=None, users_add=None, users_del=None):
        """

        https://work.weixin.qq.com/api/doc#13292

        :type chat_id: str
        :type name: str
        :type owner: str
        :type users_add: list[str]
        :type users_del: list[str]
        """
        payload = {
            'chatid': chat_id,
            'name': name,
            'owner': owner,
            'add_user_list': users_add,
            'del_user_list': users_del
        }
        payload = {k: v for k, v in payload.items() if v is not None}
        self._invoke('appchat/update', payload)

    def get(self, chap_id):
        """

        https://work.weixin.qq.com/api/doc#13293

        :type chap_id: str
        :rtype: dict
        """
        return self._invoke('appchat/get', chatid=chap_id)['chat_info']

    def _send(self, msg_type, msg, safe, chat_id):
        """

        :type chat_id: str
        """
        payload = {
            'chatid': chat_id,
            'msgtype': msg_type,
            msg_type: msg
        }
        if safe:
            payload[safe] = 1
        self._invoke('appchat/send', payload)
