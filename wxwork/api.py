import time
from threading import Lock

from . import http
from .config import Config
from .errors import APIError


class API:
    def __init__(self, config=None):
        """

        :type config: Config
        """
        if not config:
            config = Config.get_default()
        else:
            try:
                config.access_token_manager.get_access_token
            except AttributeError:
                raise TypeError("expected a Config object")
        self._conf = config
        self._at_manager = config.access_token_manager

    def _invoke(self, api_path, json=None, file=None, **query):
        """Invoke specified API

        :param str api_path: API endpoint
        :param dict json: JSON data
        :param tuple file: file to upload, (will be ignored if ``json`` is provided)
        :rtype: dict
        """

        query["access_token"] = self._at_manager.access_token
        if json or file:
            body, headers = http.prepare_request(json, file)
        else:
            body, headers = None, None

        retry = False
        while True:
            result = http.request(api_path, query, body, headers)
            err_code = result["errcode"]
            if err_code in (40014, 42001) and not retry:
                # 40014: invalid access_token
                # 42001: access_token has expired
                query["access_token"] = self._at_manager.get_access_token(
                    query["access_token"])
                retry = True
            else:
                break
        if err_code != 0:
            raise APIError(**result)
        return result

    def _gen_url(self, api_path, **query):
        query['access_token'] = self._at_manager.access_token
        return http.gen_full_url(api_path, query)


class AccessTokenManager:
    API_PATH = "gettoken"

    def __init__(self, corp_id, secret):
        self.corp_id = corp_id
        self.secret = secret
        self.expires_at = 0
        self._access_token = None
        self._lock = Lock()
        self._request_args = (
            self.API_PATH, {"corpid": corp_id, "corpsecret": secret},
            None, None
        )

    @property
    def access_token(self):
        return self.get_access_token()

    def get_access_token(self, force_update=None):
        """Exchange CorpID and Secret for AccessToken

        https://work.weixin.qq.com/api/doc#10013

        :param str force_update: Invalidate corresponding cache and renew from WeChat API
        :rtype: str
        """
        if force_update:
            if force_update != self._access_token:
                return self._access_token
        elif time.time() < self.expires_at:
            return self._access_token

        with self._lock:
            # Double-checked locking
            if force_update:
                if force_update != self._access_token:
                    return self._access_token
            elif time.time() < self.expires_at:
                return self._access_token

            result = http.request(*self._request_args)
            if 'access_token' in result:
                self.expires_at = int(time.time()) + result["expires_in"]
                self._access_token = result['access_token']
                return result['access_token']
            else:
                raise APIError(**result)
