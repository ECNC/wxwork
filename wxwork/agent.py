from .api import API
from .errors import ArgumentError


class Menu(API):
    """Custom menu management"""

    def __init__(self, config=None, agent_id=None):
        super().__init__(config)
        if agent_id is None:
            agent_id = self._conf.agent_id
        self.agent_id = agent_id

    @staticmethod
    def gen_button(name, sub_button=None, btn_type=None, value=None):
        """Generate well-formed dict representing a button in custom menu

        :rtype: dict
        :type name: str
        :type sub_button: list[dict]
        :type btn_type: str
        :param str value: Required when type specified, url for 'view', key for the others
        """
        if sub_button:
            return {"name": name, "sub_button": sub_button}
        if not value:
            raise TypeError("Missing 1 required argument: 'value'")
        if btn_type in ("click", "scancode_push", "scancode_waitmsg", "pic_sysphoto",
                        "pic_photo_or_album", "pic_weixin", "location_select"):
            return {"name": name, "type": btn_type, "key": value}
        elif btn_type == "view":
            return {"name": name, "type": "view", "url": value}
        else:
            raise ArgumentError("Invalid button type '%s'" % btn_type)

    def set(self, menu):
        """Create menu for App

        :type menu: list[dict]
        """
        self._invoke("menu/create", {"button": menu}, agentid=self.agent_id)

    def delete(self):
        """Delete the App's menu"""
        self._invoke("menu/delete", agentid=self.agent_id)

    def get(self):
        """

        :rtype: list
        """
        return self._invoke("menu/get", agentid=self.agent_id)["button"]


class Agent(API):
    """Agent management

    https://work.weixin.qq.com/api/doc#10025
    """

    def set(self, agent_id, name=None, description=None, logo_media_id=None,
            is_report_enter=None, is_report_location=None, redirect_domain=None,
            home_url=None):
        """

        :type agent_id: int
        :type name: str
        :type description: str
        :type logo_media_id: str
        :param bool is_report_enter: Enable enter-agent event
        :param bool is_report_location: 0: Disabled, 1: Report when enter
        :param str redirect_domain: Trusted domain for agent
        :param str home_url: URL for homepage type agent
        """
        if home_url and not home_url.startswith(("http://", "https://")):
            raise ArgumentError("home_url must begins with a scheme name (http or https)")
        options = {
            "agentid": agent_id,
            "description": description,
            "home_url": home_url,
            "isreportenter": None if is_report_enter is None else int(is_report_enter),
            "logo_mediaid": logo_media_id,
            "name": name,
            "redirect_domain": redirect_domain,
            "report_location_flag": None if is_report_location is None else int(is_report_location)
        }
        payload = {k: v for k, v in options.items() if v is not None}
        self._invoke("agent/set", payload)

    def get(self, agent_id):
        """

        :rtype: dict
        """
        return self._invoke("agent/get", agentid=agent_id)

    def get_list(self):
        """

        :rtype: list
        """
        return self._invoke("agent/list")["agentlist"]
