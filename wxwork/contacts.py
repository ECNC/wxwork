from enum import Enum

from .api import API
from .errors import ArgumentError


class UpdateMode(Enum):
    INCREMENTAL = 'syncuser'
    OVERWRITE_USERS = 'replaceuser'
    OVERWRITE_STRUCTURE = 'replaceparty'


class _TrackedList(list):
    __slots__ = ('_modified',)

    def __init__(self, l, modified=False):
        list.__init__(self, l)
        self._modified = modified

    def __getstate__(self):
        return self._modified

    def __setstate__(self, state):
        self._modified = state

    def __setitem__(self, i, o):
        list.__setitem__(self, i, o)
        self._modified = True

    def __delitem__(self, i):
        list.__delitem__(self, i)
        self._modified = True

    def __iadd__(self, x):
        list.__iadd__(self, x)
        self._modified = True

    def __imul__(self, n):
        list.__imul__(self, n)
        self._modified = True

    def append(self, o):
        list.append(self, o)
        self._modified = True

    def clear(self):
        list.clear(self)
        self._modified = True

    def extend(self, iterable):
        list.extend(self, iterable)
        self._modified = True

    def insert(self, i, o):
        list.insert(self, i, o)
        self._modified = True

    def pop(self, i=-1):
        self._modified = True
        return list.pop(self, i)

    def remove(self, o):
        list.remove(self, o)
        self._modified = True


class _User:
    def __init__(self, payload, is_new):
        self._payload = payload
        self._is_new = is_new

    def _set(self, key, value):
        raise NotImplementedError

    @property
    def alias(self):
        return self._payload.get('alias')

    @alias.setter
    def alias(self, alias):
        if alias is None:
            alias = ''
        elif not 0 <= len(alias) <= 32:
            raise ArgumentError("'alias' must be between 0 and 32 characters")
        self._set('alias', alias)

    @property
    def avatar_media_id(self):
        if self._is_new:
            return self._payload.get('avatar_mediaid')
        if 'avatar_mediaid' in self._payload:
            return self._payload['avatar_mediaid']
        raise AttributeError("'User' object has no attribute 'avatar_media_id'")

    @avatar_media_id.setter
    def avatar_media_id(self, avatar_media_id):
        if avatar_media_id is None:
            avatar_media_id = ''
        self._set('avatar_mediaid', avatar_media_id)

    @property
    def department(self):
        return self._payload['department']

    @department.setter
    def department(self, department):
        if isinstance(department, int):
            department = [department]
        self._set('department', department)

    @property
    def email(self):
        return self._payload.get('email')

    @email.setter
    def email(self, email):
        if email is None or email == '':
            email = ''
        elif '@' not in email or '.' not in email:
            raise ArgumentError('invalid email address')
        elif not 0 <= len(email.encode()) <= 64:
            raise ArgumentError(
                "size of 'email' must be between 1 and 64 bytes")
        self._set('email', email)

    @property
    def extended_attr(self):
        return self._payload['extattr']['attrs'] \
            if 'extattr' in self._payload else None

    @extended_attr.setter
    def extended_attr(self, attr):
        if not attr:
            if self._is_new:
                attr = ''
            else:
                attrs = self.extended_attr
                if not attrs:
                    return
                for i in attrs:
                    if 'value' in i:
                        del i['value']
                attr = self._payload['extattr']
        elif isinstance(attr, dict):
            if 'attrs' not in attr:
                attr = {'attrs': [attr]}
        elif isinstance(attr, (list, tuple)):
            attr = {'attrs': attr}
        else:
            raise TypeError(
                "'extended_attr' must be a dict, a list or a tuple, not '%s'"
                % type(attr).__name__
            )
        self._set('extattr', attr)

    @property
    def external_attr(self):
        return self._payload['external_profile']['external_attr'] \
            if 'external_profile' in self._payload else None

    @external_attr.setter
    def external_attr(self, attr):
        if not attr:
            if self._is_new:
                attr = ''
            else:
                attrs = self.external_attr
                if not attrs:
                    return
                attr = {'external_attr': [
                    {'type': i['type'], 'name': i['name']} for i in attrs
                ]}
        elif isinstance(attr, dict):
            if 'external_attr' not in attr:
                attr = {'external_attr': [attr]}
        elif isinstance(attr, (list, tuple)):
            attr = {'external_attr': attr}
        else:
            raise TypeError(
                "'external_attr' must be a dict, a list or a tuple, not '%s'"
                % type(attr).__name__
            )
        self._set('external_profile', attr)

    @property
    def external_position(self):
        return self._payload.get('external_position')

    @external_position.setter
    def external_position(self, external_position):
        if external_position is None:
            external_position = ''
        elif not 0 <= len(external_position) <= 12:
            raise ArgumentError(
                "'external_position' must be between 0 and 12 characters")
        self._set('external_position', external_position)

    @property
    def gender(self):
        gender = self._payload.get('gender')
        return int(gender) if gender else None

    @gender.setter
    def gender(self, gender):
        if gender is None:
            gender = ''
        else:
            gender = str(gender)
        self._set('gender', gender)

    @property
    def is_enabled(self):
        return bool(self._payload.get('enable', True))

    @is_enabled.setter
    def is_enabled(self, is_enabled):
        self._set('enable', int(is_enabled))

    @property
    def is_leader(self):
        return bool(self._payload.get('isleader'))

    @is_leader.setter
    def is_leader(self, is_leader):
        self._set('isleader', int(is_leader))

    @property
    def mobile(self):
        return self._payload.get('mobile')

    @mobile.setter
    def mobile(self, mobile):
        if mobile is None or mobile == '':
            mobile = ''
        elif mobile[0] not in '+0123456789':
            raise ArgumentError('invalid mobile')
        self._set('mobile', mobile)

    @property
    def name(self):
        return self._payload['name']

    @name.setter
    def name(self, name):
        if 1 <= len(name) <= 64:
            self._set('name', name)
        else:
            raise ArgumentError("'name' must be between 1 and 64 characters")

    @property
    def order(self):
        return self._payload.get('order')

    @order.setter
    def order(self, order):
        department = self.department
        if order is None:
            if self._is_new:
                order = ''
            else:
                order = (0,) * len(department)
        else:
            if isinstance(order, int):
                order = (order,)
            if len(department) != len(order):
                raise ArgumentError(
                    "'order' must be the same length as 'department'")
        self._set('order', order)

    @property
    def position(self):
        return self._payload.get('position')

    @position.setter
    def position(self, position):
        if position is None:
            position = ''
        elif not 0 <= len(position) <= 128:
            raise ArgumentError(
                "'position' must be between 0 and 128 characters")
        self._set('position', position)

    @property
    def telephone(self):
        return self._payload.get('telephone')

    @telephone.setter
    def telephone(self, telephone):
        if telephone is None:
            telephone = ''
        elif not 0 <= len(telephone.encode()) <= 32:
            raise ArgumentError(
                "'telephone' must be between 0 and 32 characters")
        self._set('telephone', telephone)

    @property
    def user_id(self):
        return self._payload['userid']


class FetchedUser(_User):
    def __init__(self, payload):
        super().__init__(payload, False)
        self._updated_fields = set()
        payload['department'] = _TrackedList(payload['department'])
        payload['order'] = tuple(payload['order'])

    def _set(self, key, value):
        self._payload[key] = value
        self._updated_fields.add(key)

    @_User.extended_attr.getter
    def extended_attr(self):
        attrs = self._payload['extattr']['attrs']
        if attrs:
            self._updated_fields.add('extattr')
            return attrs

    @_User.external_attr.getter
    def external_attr(self):
        attrs = self._payload['external_profile']['external_attr'] \
            if 'external_profile' in self._payload else None
        if attrs:
            self._updated_fields.add('external_profile')
            return attrs

    @property
    def avatar(self):
        return self._payload['avatar']

    @property
    def qr_code(self):
        return self._payload['qr_code']

    @property
    def status(self):
        return self._payload['status']

    def _to_payload(self):
        if 'department' not in self._updated_fields \
                and self.department._modified:
            self._updated_fields.add('department')
        source = self._payload
        payload = {i: source[i] for i in self._updated_fields}
        payload['userid'] = source['userid']
        return payload


class User(_User):
    def __init__(self, user_id, name, department=(1,), email=None, mobile=None,
                 *, order=None, alias=None, position=None, gender=None,
                 telephone=None, is_leader=None, is_enabled=None,
                 avatar_media_id=None, extended_attr=None, external_attr=None,
                 external_position=None):
        """

        :param str user_id: Unique UserID in contacts, 1~64 bytes of case-insensitive ASCII
        :type name: str
        :type department: int or list[int]
        :type email: str
        :type mobile: str
        :type order: int or tuple[int]
        :type alias: str
        :type position: str
        :param int gender: 1 for male, 2 for female
        :type telephone: str
        :type is_leader: bool
        :type is_enabled: bool
        :type avatar_media_id: str
        :type extended_attr: list[dict[str, str]] or dict
        :type external_attr: list[dict] or dict
        :type external_position: str
        """
        super().__init__({}, True)
        self.user_id = user_id
        self.name = name
        self.department = department
        if not (email or mobile):
            raise ArgumentError("'email' and 'mobile' cannot both be empty")
        if email:
            self.email = email
        if mobile:
            self.mobile = mobile
        if order:
            self.order = order
        if alias:
            self.alias = alias
        if position:
            self.position = position
        if gender:
            self.gender = gender
        if telephone:
            self.telephone = telephone
        if is_leader:
            self.is_leader = is_leader
        if is_enabled is not None:
            self.is_enabled = is_enabled
        if avatar_media_id:
            self.avatar_media_id = avatar_media_id
        if extended_attr:
            self.extended_attr = extended_attr
        if external_attr:
            self.external_attr = external_attr
        if external_position:
            self.external_position = external_position

    def _set(self, key, value):
        if value != '':
            self._payload[key] = value
        elif key in self._payload:
            del self._payload[key]

    @property
    def user_id(self):
        return self._payload['userid']

    @user_id.setter
    def user_id(self, user_id):
        if 1 <= len(user_id.encode()) <= 64:
            self._payload['userid'] = user_id
        else:
            raise ArgumentError(
                "size of 'user_id' must be between 1 and 64 bytes")


class Contacts(API):
    """Contacts management

    https://work.weixin.qq.com/api/doc#10016
    """
    UpdateMode = UpdateMode
    User = User

    def async_update(self, mode, media_id, callback=False, to_invite=False):
        """

        :type mode: UpdateMode or str
        :type media_id: str
        :type callback: bool
        :type to_invite: bool
        :rtype: str
        """
        mode = UpdateMode(mode)
        payload = {
            'media_id': media_id
        }
        if mode is not UpdateMode.OVERWRITE_STRUCTURE:
            payload['to_invite'] = to_invite
        if callback:
            _, token, enc_aes_key = self._conf.get_callback_param()
            callback_url = self._conf.callback_url
            payload['callback'] = {
                'url': callback_url,
                'token': token,
                'encodingaeskey': enc_aes_key
            }
        return self._invoke('batch/%s' % mode.value, payload)['jobid']

    def get_async_result(self, job_id):
        """

        :type job_id: str
        :rtype: dict
        """
        return self._invoke('batch/getresult', jobid=job_id)

    def grant_auth(self, user_id):
        """

        :type user_id: str
        """
        self._invoke('user/authsucc', userid=user_id)

    def add_user(self, user_id, name, department=(1,), mobile=None, email=None,
                 alias=None, order=None, position=None, gender=None,
                 telephone=None, is_leader=None, avatar_media_id=None,
                 is_enabled=None, ext_attr=None, to_invite=False,
                 external_profile=None, external_position=None):
        """Create a new user

        :param str user_id: Unique UserID in contacts, 1~64 bytes of case-insensitive ASCII
        :type name: str
        :type department: int or list[int]
        :type mobile: str
        :type email: str
        :type alias: str
        :type order: int or tuple[int]
        :type position: str
        :param int gender: 1 for male, 2 for female
        :type telephone: str
        :type is_leader: bool
        :type avatar_media_id: str
        :type is_enabled: bool
        :type ext_attr: list[dict[str, str]]
        :type to_invite: bool
        :type external_profile: list[dict]
        :type external_position: str
        """
        if isinstance(department, int):
            department = (department,)

        payload = {
            'userid': user_id,
            'name': name,
            'department': department,
            'to_invite': to_invite
        }
        if mobile:
            payload['mobile'] = mobile
        if email:
            payload['email'] = email
        if alias:
            payload['alias'] = alias
        if order:
            if isinstance(order, int):
                order = (order,)
            payload['order'] = order
        if position:
            payload['position'] = position
        if gender:
            payload['gender'] = str(gender)
        if telephone:
            payload['telephone'] = telephone
        if is_leader:
            payload['isleader'] = is_leader
        if avatar_media_id:
            payload['avatar_mediaid'] = avatar_media_id
        if is_enabled is not None:
            payload['enable'] = int(is_enabled)
        if ext_attr:
            payload['extattr'] = {'attrs': ext_attr}
        if external_profile:
            payload['external_profile'] = {'external_attr': external_profile}
        if external_position:
            payload['external_position'] = external_position

        self._invoke('user/create', payload)

    def add_user_object(self, user, to_invite=False):
        """

        :type user: User
        :type to_invite: bool
        """
        payload = user._payload
        payload['to_invite'] = to_invite
        self._invoke('user/create', payload)

    def get_user(self, user_id):
        """Retrieve a user's details

        :rtype: dict
        """
        return self._invoke('user/get', userid=user_id)

    def get_user_object(self, user_id):
        """

        :type user_id: str
        """
        return FetchedUser(self._invoke('user/get', userid=user_id))

    def update_user(self, user_id, name=None, department=None, mobile=None,
                    email=None, alias=None, order=None, position=None,
                    gender=None, telephone=None, is_leader=None,
                    avatar_media_id=None, is_enabled=None, ext_attr=None,
                    external_profile=None, external_position=None):
        """

        :type user_id: str
        :type name: str
        :type department: int or list[int]
        :type mobile: str
        :type email: str
        :type alias: str
        :type order: int or tuple[int]
        :type position: str
        :type gender: int
        :type telephone: str
        :type is_leader: bool
        :type avatar_media_id: str
        :type is_enabled: bool
        :type ext_attr: list[dict[str, str]]
        :type external_profile: list[dict]
        :type external_position: str
        """
        payload = {
            'userid': user_id
        }
        if name:
            payload['name'] = name
        if department:
            if isinstance(department, int):
                department = (department,)
            payload['department'] = department
        if mobile is not None:
            payload['mobile'] = mobile
        if email is not None:
            payload['email'] = email
        if alias is not None:
            payload['alias'] = alias
        if order is not None:
            if isinstance(order, int):
                order = (order,)
            payload['order'] = order
        if position is not None:
            payload['position'] = position
        if gender is not None:
            payload['gender'] = str(gender)
        if telephone is not None:
            payload['telephone'] = telephone
        if is_leader is not None:
            payload['isleader'] = int(is_leader)
        if avatar_media_id is not None:
            payload['avatar_mediaid'] = avatar_media_id
        if is_enabled is not None:
            payload['enable'] = int(is_enabled)
        if ext_attr:
            payload['extattr'] = {'attrs': ext_attr}
        if external_profile:
            payload['external_profile'] = {'external_attr': external_profile}
        if external_position is not None:
            payload['external_position'] = external_position

        self._invoke('user/update', payload)

    def update_user_object(self, user):
        """

        :type user: FetchedUser
        """
        payload = user._to_payload()
        if len(payload) > 1:
            self._invoke('user/update', payload)
            user._updated_fields.clear()

    def del_user(self, user_id):
        """

        :type user_id: str or list[str]
        """
        if isinstance(user_id, str):
            self._invoke('user/delete', userid=user_id)
        else:
            payload = {'useridlist': user_id}
            self._invoke('user/batchdelete', payload)

    def del_user_object(self, user):
        """

        :type user: FetchedUser or list[FetchedUser]
        """
        if isinstance(user, FetchedUser):
            self._invoke('user/delete', userid=user.user_id)
        else:
            payload = {'useridlist': [u.user_id for u in user]}
            self._invoke('user/batchdelete', payload)

    def add_dept(self, name, parent_id=1, order=None, dept_id=None):
        """Create a new department

        :type name: str
        :param int parent_id: Parent department id, Root department id is 1
        :param int order: Order in parent department, listed in descending order
        :param int dept_id: Must greater than 1, or None for auto generated
        :rtype: int
        """
        payload = {
            'name': name,
            'parentid': parent_id
        }
        if order:
            payload['order'] = order
        if dept_id:
            payload['id'] = dept_id
        return self._invoke('department/create', payload)['id']

    def get_dept(self, dept_id=None):
        """

        :type dept_id: int
        :rtype: list[dict]
        """
        query = {'id': str(dept_id)} if dept_id else {}
        return self._invoke('department/list', **query)['department']

    def update_dept(self, dept_id, name=None, parent_id=None, order=None):
        """

        :type dept_id: int
        :type name: str
        :type parent_id: int
        :type order: int
        """
        payload = {
            'id': dept_id
        }
        if name:
            payload['name'] = name
        if parent_id:
            payload['parentid'] = parent_id
        if order is not None:
            payload['order'] = order
        self._invoke('department/update', payload)

    def del_dept(self, dept_id):
        """

        :type dept_id: int
        """
        self._invoke('department/delete', id=str(dept_id))

    def get_dept_members(self, dept_id, fetch_child=False, in_detail=False):
        """Retrieve members of a specific department

        :rtype: list
        :type dept_id: int
        :param bool fetch_child: Recursive query users in child departments
        :param bool in_detail: Get details of user
        """
        query = {
            'department_id': str(dept_id),
            'fetch_child': '1' if fetch_child else '0'
        }
        api = 'user/list' if in_detail else 'user/simplelist'
        return self._invoke(api, **query)['userlist']

    def add_tag(self, name, tag_id=None):
        """Create a new tag

        :type name: str
        :type tag_id: int
        :rtype: int
        """
        payload = {
            'tagname': name,
            'tagid': tag_id
        }
        return self._invoke('tag/create', payload)['tagid']

    def get_tag(self):
        """

        :rtype: list[dict]
        """
        return self._invoke('tag/list')['taglist']

    def update_tag(self, tag_id, name):
        """Modify a tag's name

        :type tag_id: int
        :type name: str
        """
        self._invoke('tag/update', {'tagid': tag_id, 'tagname': name})

    def del_tag(self, tag_id):
        """Delete a tag

        The tag to be deleted must have no users
        :type tag_id: int
        """
        self._invoke('tag/delete', tagid=str(tag_id))

    def add_tag_members(self, tag_id, users=None, depts=None):
        """

        :type tag_id: int
        :type users: str or list[str]
        :type depts: int or list[int]
        :rtype: dict
        """
        if not (users or depts):
            raise TypeError("Missing 1 required argument: 'users' or 'depts'")

        payload = {
            'tagid': tag_id
        }
        if users:
            if isinstance(users, str):
                users = (users,)
            payload['userlist'] = users
        if depts:
            if isinstance(depts, int):
                depts = (depts,)
            payload['partylist'] = depts

        return self._invoke('tag/addtagusers', payload)

    def get_tag_members(self, tag_id):
        """

        :type tag_id: int
        :rtype: dict
        """
        return self._invoke('tag/get', tagid=str(tag_id))

    def del_tag_members(self, tag_id, users=None, depts=None):
        """Delete members from a tag

        :type tag_id: int
        :type users: str or list[str]
        :type depts: int or list[int]
        :rtype: dict
        """
        if not (users or depts):
            raise TypeError("Missing 1 required argument: 'users' or 'depts'")

        payload = {
            'tagid': tag_id
        }
        if users:
            if isinstance(users, str):
                users = (users,)
            payload['userlist'] = users
        if depts:
            if isinstance(depts, int):
                depts = (depts,)
            payload['partylist'] = depts

        return self._invoke('tag/deltagusers', payload)
