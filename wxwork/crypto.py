import base64
from hashlib import sha1
from os import urandom
from random import getrandbits
from struct import Struct

from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.ciphers import Cipher
from cryptography.hazmat.primitives.ciphers.algorithms import AES
from cryptography.hazmat.primitives.ciphers.modes import CBC

from .errors import SignatureError


class Cryptography:
    def __init__(self, key):
        """

        :type key: bytes
        """
        self.cipher = Cipher(AES(key), CBC(key[:16]), backend=default_backend())

    @staticmethod
    def pad(data, block_size=32):
        # block_size must be 32 or Wechat refuses to accept
        ordinal = block_size - (len(data) % block_size)
        padding = bytes((ordinal,)) * ordinal
        return data + padding

    @staticmethod
    def unpad(data):
        ordinal = data[-1]
        return data[:-ordinal]

    def encrypt(self, plain):
        encryptor = self.cipher.encryptor()
        cipher = encryptor.update(self.pad(plain)) + encryptor.finalize()
        return base64.b64encode(cipher)

    def decrypt(self, cipher):
        decryptor = self.cipher.decryptor()
        plain = decryptor.update(base64.b64decode(cipher)) + decryptor.finalize()
        return self.unpad(plain)


class Wrapper:
    XML_TPL = ('<xml>'
               '<Encrypt>%s</Encrypt>'
               '<MsgSignature>%s</MsgSignature>'
               '<TimeStamp>%s</TimeStamp>'
               '<Nonce>%s</Nonce>'
               '</xml>')
    size_struct = Struct('>i')

    def __init__(self, corp_id, token, encoded_aes_key):
        """

        :type corp_id: str
        :type token: str
        :type encoded_aes_key: str
        """
        aes_key = base64.b64decode(encoded_aes_key + '=')
        self.corp_id = corp_id.encode()
        self.token = token.encode()
        self.cryptor = Cryptography(aes_key)

    def sign(self, cipher, timestamp, nonce):
        """

        :type cipher: bytes
        :type timestamp: str
        :type nonce: str
        :rtype: str
        """
        data = [self.token, timestamp.encode(), nonce.encode(), cipher]
        data.sort()
        signature = sha1(b''.join(data)).hexdigest()
        return signature

    def encapsulate(self, plain, timestamp):
        """

        :type plain: str
        :type timestamp: int
        :rtype: str
        """
        plain = plain.encode()
        string = b''.join((
            urandom(16), self.size_struct.pack(len(plain)), plain, self.corp_id
        ))
        cipher = self.cryptor.encrypt(string)
        timestamp = str(timestamp)
        nonce = str(getrandbits(31))
        msg_signature = self.sign(cipher, timestamp, nonce)
        return self.XML_TPL % (cipher.decode(), msg_signature, timestamp, nonce)

    def extract(self, cipher, msg_signature, timestamp, nonce):
        """

        :type cipher: str
        :type msg_signature: str
        :type timestamp: str
        :type nonce: str
        :rtype: bytes
        """
        cipher = cipher.encode()
        if self.sign(cipher, timestamp, nonce) == msg_signature:
            plain = self.cryptor.decrypt(cipher)
            data_size = self.size_struct.unpack(plain[16:20])[0]
            return plain[20:data_size + 20]
        else:
            raise SignatureError("Invalid signature")
