import json
import os
import ssl
from random import getrandbits
from urllib.parse import quote

import urllib3

from .errors import HTTPError

API_HOST = "qyapi.weixin.qq.com"
API_ROOT = "/cgi-bin/"
POOL = urllib3.HTTPSConnectionPool(API_HOST,
                                   timeout=urllib3.Timeout(connect=3.05),
                                   maxsize=3,
                                   retries=urllib3.Retry(2),
                                   cert_reqs=ssl.CERT_REQUIRED)

JSON_DECODER = json.JSONDecoder()
JSON_ENCODER = json.JSONEncoder(
    ensure_ascii=False, separators=(',', ':'),
    check_circular=os.environ.get("WXWORK_DISABLE_CIRCULAR_CHECK") != '1'
)

GET_HEADERS = {"Connection": "keep-alive"}
POST_JSON_HEADERS = {"Connection": "keep-alive",
                     "Content-Type": "application/json"}
POST_FILE_HEADERS = {"Connection": "keep-alive", "Content-Type": None}

URL_FORMAT = API_ROOT + "%s?%s"


def gen_full_url(path, query):
    """Generate full url for a specific API request

    :type path: str
    :type query: dict[str, str]
    :rtype: str
    """
    url = 'https://{host}{path}?{query}'.format(
        host=API_HOST, path=API_ROOT + path,
        query='&'.join(['%s=%s' % i for i in query.items()])
    )
    return url


def request(path, query, body, headers):
    """Request WeChat Work API with given args

    :param str path: API path without root dir (/cgi-bin/)
    :type query: dict[str, str]
    :param body: bytes-like object
    :type headers: dict[str, str] or None
    :rtype: dict
    """
    if body:
        method = "POST"
    else:
        method = "GET"
        headers = GET_HEADERS
    url = URL_FORMAT % (path, "&".join(["%s=%s" % i for i in query.items()]))
    try:
        response = POOL.urlopen(method, url, body, headers,
                                assert_same_host=False)
    except urllib3.exceptions.HTTPError as e:
        raise HTTPError(
            "Something unexpected happened while sending API request") from e
    if response.status != 200:
        raise HTTPError(
            "Unexpected HTTP status code: %s, response body:\n%s" %
            (response.status, response.data))
    try:
        result = JSON_DECODER.decode(response.data.decode())
    except ValueError as e:
        raise HTTPError("Invalid response: %s" % response.data) from e
    if result.__class__ is dict:
        return result
    else:
        raise HTTPError("Invalid response: %s" % result)


def encode_multipart(filename, data, mime):
    """Encode a file using multipart/form-data defined by RFC 7578

    :type filename: str
    :type data: bytes
    :type mime: str
    :rtype: tuple[bytearray, str]
    """
    boundary = '----WXWorkFormBoundary%08x' % getrandbits(32)
    delimiter = ('--' + boundary).encode()

    body = bytearray()
    body.extend(delimiter)
    body.extend(b'\r\n')
    body.extend(b'Content-Disposition: form-data; name="media"; filename="')
    body.extend(quote(filename).encode())
    body.extend(b'"\r\nContent-Type: ')
    body.extend(mime.encode())
    body.extend(b'\r\n\r\n')
    body.extend(data)
    body.extend(b'\r\n')
    body.extend(delimiter)
    body.extend(b'--\r\n')
    content_type = 'multipart/form-data; boundary=' + boundary
    return body, content_type


def prepare_request(obj, file):
    """Generate POST request body and headers

    :type obj: dict
    :param file: a file represented as (filename, data, mime)
    :type file: tuple
    :return: body, headers
    """
    if obj:
        headers = POST_JSON_HEADERS
        body = JSON_ENCODER.encode(obj).encode()
    else:
        headers = POST_FILE_HEADERS.copy()
        body, headers["Content-Type"] = encode_multipart(*file)
    return body, headers
