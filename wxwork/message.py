class Basis:
    def __init__(self, corp_id, user, create_time):
        """

        :param str corp_id: <ToUserName> CorpID
        :param str user: <FromUserName> UserID of sender or 'sys' for system generated event
        :param int create_time: <CreateTime> Message create time
        """
        self.corp_id = corp_id
        self.user = user
        self.create_time = create_time


class Event(Basis):
    type = 'event'
    event = None

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.create_time == other.create_time and self.user == other.user
        elif isinstance(other, Basis):
            return False
        else:
            return NotImplemented

    def __hash__(self):
        return hash((self.create_time, self.user, self.event))


class UserAction(Basis):
    def __init__(self, corp_id, user, create_time, agent_id):
        """

        :param int agent_id: <AgentID>
        """
        super().__init__(corp_id, user, create_time)
        self.agent_id = agent_id


class UserEvent(Event, UserAction):
    pass


class Menu(UserEvent):
    def __init__(self, corp_id, user, create_time, agent_id, event_key):
        """

        :param str event_key: <EventKey>
        """
        super().__init__(corp_id, user, create_time, agent_id)
        self.event_key = event_key


class Message(UserAction):
    def __init__(self, corp_id, user, create_time, agent_id, msg_id):
        """

        :param int msg_id: <MsgId>
        """
        super().__init__(corp_id, user, create_time, agent_id)
        self.msg_id = msg_id

    def __eq__(self, other):
        if isinstance(other, Message):
            return self.msg_id == other.msg_id
        elif isinstance(other, Basis):
            return False
        else:
            return NotImplemented

    def __hash__(self):
        return hash(self.msg_id)


class Media(Message):
    def __init__(self, corp_id, user, create_time, agent_id, msg_id, media_id):
        """

        :param str media_id: <MediaId>
        """
        super().__init__(corp_id, user, create_time, agent_id, msg_id)
        self.media_id = media_id


class Text(Message):
    type = 'text'

    def __init__(self, corp_id, user, create_time, agent_id, msg_id, content):
        """

        :param str content: <Content>
        """
        super().__init__(corp_id, user, create_time, agent_id, msg_id)
        self.content = content


class Image(Media):
    type = 'image'

    def __init__(self, corp_id, user, create_time, agent_id, msg_id, media_id,
                 image_url):
        """

        :param str image_url: <PicUrl> image URL
        """
        super().__init__(corp_id, user, create_time, agent_id, msg_id, media_id)
        self.image_url = image_url


class Voice(Media):
    type = 'voice'

    def __init__(self, corp_id, user, create_time, agent_id, msg_id, media_id,
                 audio_format):
        """

        :param str audio_format: <Format> Audio compression format (amr, speex, ...)
        """
        super().__init__(corp_id, user, create_time, agent_id, msg_id, media_id)
        self.audio_format = audio_format


class Video(Media):
    type = 'video'

    def __init__(self, corp_id, user, create_time, agent_id, msg_id, media_id,
                 thumb_media_id):
        """

        :param str thumb_media_id: <ThumbMediaId> MediaID of video thumbnail
        """
        super().__init__(corp_id, user, create_time, agent_id, msg_id, media_id)
        self.thumb_media_id = thumb_media_id


class Location(Message):
    type = 'location'

    def __init__(self, corp_id, user, create_time, agent_id, msg_id,
                 latitude, longitude, scale, label):
        """

        :param float latitude: <Location_X>
        :param float longitude: <Location_Y>
        :param int scale: <Scale>
        :param str label: <Label> Optional location label (Address)
        """
        super().__init__(corp_id, user, create_time, agent_id, msg_id)
        self.latitude = latitude
        self.longitude = longitude
        self.scale = scale
        self.label = label


class Link(Message):
    type = 'link'

    def __init__(self, corp_id, user, create_time, agent_id, msg_id,
                 title, description, url, thumb_url):
        """

        :param str title: <Title>
        :param str description: <Description>
        :param str url: <Url>
        :param str thumb_url: <PicUrl> URL for cover thumbnail
        """
        super().__init__(corp_id, user, create_time, agent_id, msg_id)
        self.title = title
        self.description = description
        self.url = url
        self.thumb_url = thumb_url


class Subscribe(UserEvent):
    event = 'subscribe'


class Unsubscribe(UserEvent):
    event = 'unsubscribe'


class EnterAgent(UserEvent):
    event = 'enter_agent'


class Click(Menu):
    event = 'click'


class View(Menu):
    event = 'view'


class LocationReport(UserEvent):
    event = 'location'

    def __init__(self, corp_id, user, create_time, agent_id,
                 latitude, longitude, precision):
        """

        :param float latitude: <Latitude>
        :param float longitude: <Longitude>
        :param float precision: <Precision>
        """
        super().__init__(corp_id, user, create_time, agent_id)
        self.latitude = latitude
        self.longitude = longitude
        self.precision = precision


class ScanCode(Menu):
    event = 'scancode'

    def __init__(self, corp_id, user, create_time, agent_id, event_key,
                 code_type, data):
        """

        :param str code_type: <ScanType> (barcode, qrcode)
        :param str data: <ScanResult> 'coding_type,data' for barcode
                                          (type: EAN_13, DATA_MATRIX, ...)
                                      'data' for QR code
        """
        super().__init__(corp_id, user, create_time, agent_id, event_key)
        self.code_type = code_type
        self.data = data


class Pic(Menu):
    event = 'pic'

    def __init__(self, corp_id, user, create_time, agent_id, event_key,
                 image_md5_list):
        """

        :param list[str] image_md5_list: <PicList>
        """
        super().__init__(corp_id, user, create_time, agent_id, event_key)
        self.image_md5_list = image_md5_list

    @property
    def count(self):
        return len(self.image_md5_list)


class LocationSelect(Menu):
    event = 'location_select'

    def __init__(self, corp_id, user, create_time, agent_id, event_key,
                 latitude, longitude, scale, label, poi_name):
        """

        :param float latitude: <Location_X>
        :param float longitude: <Location_Y>
        :param int scale: <Scale>
        :param str label: <Label>
        :param str poi_name: <Poiname>
        """
        super().__init__(corp_id, user, create_time, agent_id, event_key)
        self.latitude = latitude
        self.longitude = longitude
        self.scale = scale
        self.label = label
        self.poi_name = poi_name


class JobResult(Event):
    event = 'batch_job_result'

    def __init__(self, corp_id, user, create_time,
                 job_id, job_type, err_code, err_msg):
        """

        :param str job_id: <JobId>
        :param str job_type: <JobType>
        :param int err_code: <ErrCode>
        :param str err_msg: <ErrMsg>
        """
        super().__init__(corp_id, user, create_time)
        self.job_id = job_id
        self.job_type = job_type
        self.err_code = err_code
        self.err_msg = err_msg


class Contacts(Event):
    event = 'change_contact'
    change_type = None


class Response:
    TYPE = None
    TPL_BASE = ('<xml>'
                '<ToUserName>%s</ToUserName>'
                '<FromUserName>%s</FromUserName>'
                '<CreateTime>%s</CreateTime>'
                '<MsgType>%s</MsgType>'
                '%s'
                '</xml>')

    def __init__(self, source):
        """

        :type source: UserAction
        """
        self.source = source
        self.create_time = source.create_time

    def _build_body(self):
        raise NotImplementedError

    def to_xml(self):
        return Response.TPL_BASE % (
            self.source.user, self.source.corp_id, self.create_time,
            self.TYPE, self._build_body()
        )


def _escape_cdata(data):
    return data.replace(']]>', ']]]]><![CDATA[>')


class MediaResponse(Response):
    TPL_MEDIA = ('<%s>'
                 '<MediaId><![CDATA[%s]]></MediaId>'
                 '%s'
                 '</%s>')
    TPL_BODY = None

    def __init__(self, source, media_id):
        """

        :type media_id: str
        """
        super().__init__(source)
        self.media_id = media_id

    def _build_body(self):
        media_tag = self.TYPE.capitalize()
        return MediaResponse.TPL_MEDIA % (
            media_tag, self.media_id,
            '%s' if self.TPL_BODY else '',
            media_tag
        )


class TextResponse(Response):
    TYPE = 'text'
    TPL_BODY = '<Content><![CDATA[%s]]></Content>'

    def __init__(self, source, content):
        """

        :type content: str
        """
        super().__init__(source)
        self.content = content

    def _build_body(self):
        return TextResponse.TPL_BODY % _escape_cdata(self.content)


class ImageResponse(MediaResponse):
    TYPE = 'image'


class VoiceResponse(MediaResponse):
    TYPE = 'voice'


class VideoResponse(MediaResponse):
    TYPE = 'video'
    TPL_BODY = ('<Title><![CDATA[%s]]></Title>'
                '<Description><![CDATA[%s]]></Description>')

    def __init__(self, source, media_id, title, description):
        """

        :type title: str
        :type description: str
        """
        super().__init__(source, media_id)
        self.title = title
        self.description = description

    def _build_body(self):
        return super()._build_body() % (
            VideoResponse.TPL_BODY % (
                _escape_cdata(self.title),
                _escape_cdata(self.description)
            )
        )


class NewsResponse(Response):
    TYPE = 'news'
    TPL_NEWS = ('<item>'
                '<Title><![CDATA[%s]]></Title>'
                '<Description><![CDATA[%s]]></Description>'
                '<PicUrl><![CDATA[%s]]></PicUrl>'
                '<Url><![CDATA[%s]]></Url>'
                '</item>')
    TPL_BODY = ('<ArticleCount>%s</ArticleCount>'
                '<Articles>'
                '%s'
                '</Articles>')

    def __init__(self, source, articles):
        """

        :type articles: list[dict]
        """
        super().__init__(source)
        self.articles = articles

    def _build_body(self):
        articles = [
            NewsResponse.TPL_NEWS % (
                _escape_cdata(i['title']), _escape_cdata(i['description']),
                _escape_cdata(i['pic_url']), _escape_cdata(i['url'])
            )
            for i in self.articles
        ]
        return NewsResponse.TPL_BODY % (len(articles), ''.join(articles))


def parse(e):
    corp_id = e[0].text
    user = e[1].text
    create_time = int(e[2].text)
    msg_type = e[3].text

    if msg_type == 'event':
        event = e.findtext('Event').lower()
        if event == 'enter_agent':
            return EnterAgent(corp_id, user, create_time, int(e.findtext('AgentID')))
        elif event == 'location':
            return LocationReport(
                corp_id, user, create_time, int(e.findtext('AgentID')),
                float(e.findtext('Latitude')), float(e.findtext('Longitude')),
                float(e.findtext('Precision'))
            )
        elif event == 'click':
            return Click(corp_id, user, create_time, int(e.findtext('AgentID')),
                         e.findtext('EventKey'))
        elif event == 'view':
            return View(corp_id, user, create_time, int(e.findtext('AgentID')),
                        e.findtext('EventKey'))
        elif event == 'location_select':
            return LocationSelect(
                corp_id, user, create_time, int(e.findtext('AgentID')), e.findtext('EventKey'),
                float(e.findtext('SendLocationInfo/Location_X')),
                float(e.findtext('SendLocationInfo/Location_Y')),
                int(e.findtext('SendLocationInfo/Scale')),
                e.findtext('SendLocationInfo/Label'),
                e.findtext('SendLocationInfo/Poiname')
            )
        elif event == 'subscribe':
            return Subscribe(corp_id, user, create_time, int(e.findtext('AgentID')))
        elif event == 'unsubscribe':
            return Unsubscribe(corp_id, user, create_time, int(e.findtext('AgentID')))
        elif event in ('scancode_push', 'scancode_waitmsg'):
            return ScanCode(
                corp_id, user, create_time, int(e.findtext('AgentID')), e.findtext('EventKey'),
                e.findtext('ScanCodeInfo/ScanType'), e.findtext('ScanCodeInfo/ScanResult')
            )
        elif event in ('pic_sysphoto', 'pic_weixin', 'pic_photo_or_album'):
            return Pic(
                corp_id, user, create_time, int(e.findtext('AgentID')), e.findtext('EventKey'),
                [i.text for i in e.iterfind('SendPicsInfo/PicList/item/PicMd5Sum')]
            )
        elif event == 'batch_job_result':
            return JobResult(
                corp_id, user, create_time,
                e.findtext('BatchJob/JobId'), e.findtext('BatchJob/JobType'),
                int(e.findtext('BatchJob/ErrCode')), e.findtext('BatchJob/ErrMsg')
            )
    else:
        if msg_type == 'text':
            return Text(corp_id, user, create_time, int(e.findtext('AgentID')),
                        int(e.findtext('MsgId')), e.findtext('Content'))
        elif msg_type == 'image':
            return Image(corp_id, user, create_time, int(e.findtext('AgentID')),
                         int(e.findtext('MsgId')),
                         e.findtext('MediaId'), e.findtext('PicUrl'))
        elif msg_type == 'voice':
            return Voice(corp_id, user, create_time, int(e.findtext('AgentID')),
                         int(e.findtext('MsgId')),
                         e.findtext('MediaId'), e.findtext('Format'))
        elif msg_type == 'video':
            return Video(corp_id, user, create_time, int(e.findtext('AgentID')),
                         int(e.findtext('MsgId')),
                         e.findtext('MediaId'), e.findtext('ThumbMediaId'))
        elif msg_type == 'location':
            return Location(
                corp_id, user, create_time, int(e.findtext('AgentID')),
                int(e.findtext('MsgId')),
                float(e.findtext('Location_X')), float(e.findtext('Location_Y')),
                int(e.findtext('Scale')), e.findtext('Label')
            )
        elif msg_type == 'link':
            return Link(
                corp_id, user, create_time, int(e.findtext('AgentID')),
                int(e.findtext('MsgId')),
                e.findtext('Title'), e.findtext('Description'),
                e.findtext('Url'), e.findtext('PicUrl')
            )
